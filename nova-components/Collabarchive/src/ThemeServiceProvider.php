<?php

namespace Elementseven\Collabarchive;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::booted(function () {
            Nova::theme(asset('/elementseven/collabarchive/theme.css'));
        });

        $this->publishes([
            __DIR__.'/../resources/css' => public_path('elementseven/collabarchive'),
        ], 'public');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
