@php
$page = 'Contact';
$pagetitle = 'Contact | CollabArchive';
$metadescription = 'Get in touch with CollabArchive by email or using our online contact form!';
$pagetype = 'white';
$pagename = 'home';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative pt-4 mt-5 ipadp-pt-0 menu-padding">
	<div class="row">
		<div class="container container-wide">	
		  <div class="row mt-4 position-relative z-2">
		    <div class="col-lg-8 mt-5 pt-5 ipadp-pt-0 mob-mt-0 mob-pt-0 text-center text-lg-left">
		      <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
		      <h1 class="mob-mt-0">Get in touch</h1>
		      <p>It's easy to contact us, you can use the form below to send us a message or send an email to <a href="mailto:hello@collabarchive.org">hello@collabarchive.org</a>.</p>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative pb-5 mob-pb-4 position-relative z-2">
  <div class="row">
    <div class="container container-wide pt-4">
      <div class="row">
      	<div class="col-lg-10 mob-px-4">
          <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
    		</div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5 mob-mb-0"></div>

@endsection