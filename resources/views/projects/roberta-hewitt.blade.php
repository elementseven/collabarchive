@php
$page = 'Projects';
$pagetitle = $project->title . ' | CollabArchive';
$metadescription = $project->meta_description;
$pagetype = 'light';
$pagename = 'projects';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 mob-pt-4 mob-px-4">
  <div class="row top-padding">
    <div class="container container-wide">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center pt-5">
        	<p class="text-purple text-title text-uppercase mb-0"><b>{{$project->category->name}}</b></p>
          <h1 class="mb-3 text-capitalize">{{$project->title}}</h1>
          <p>{{$project->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-px-4">
  <div class="img-backdrop my-5 mob-my-3">
    <picture>
      <source srcset="{{$project->getFirstMediaUrl('projects','normal-webp')}}" type="image/webp"/> 
      <source srcset="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}"/> 
      <img src="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}" alt="{{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="927" height="573"/>
    </picture>
  </div>
</div>
<div class="container container-wide position-relative mb-5	pb-5 mob-mb-0">
	<div class="row position-relative justify-content-center z-2 mob-mb-3">
		<div class="col-lg-6 text-center text-lg-left pr-5 mob-px-3">
			<h2 class="mb-4">About the Project</h2>
			{!!$project->description!!}
		</div>
		<div class="col-lg-6">
			<div class="img-backdrop my-5 mob-mb-0">
        <picture>
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal-webp')}}" type="image/webp"/> 
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}"/> 
          <img src="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}" alt="About the project - {{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="720" height="479"/>
        </picture>
		  </div>
	  </div>
	</div>
</div>
<div class="container-fluid bg-light py-5 mob-py-0 mob-px-4">
	<div class="row">
		<div class="container py-5">
			<div class="row justify-content-center">
				<div class="col-lg-8 text-center">
					<p class="mimic-h2">Short Animated Film</p>
					<p>Our volunteers, with the support of filmmaker Dan Wilson, show you why Roberta Hewitt was more than the wife of writer John Hewitt.</p>
				</div>
			</div>
		  <div class="img-backdrop mt-5 mb-3 mob-my-3">
		    <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/vWR9neiwSCk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
		  </div>
      <p class="text-center text-small"><img src="/img/logos/cc.png" alt="Creative Commons Attribution 4.0 International License" width="80px" height="29" class="d-inline-block mr-2"/>This content is licensed under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License.</a></p>
		</div>
	</div>
</div>
<div class="container py-5 mob-px-4">
	<div class="row py-5 mob-py-0 justify-content-center">
		<div class="col-lg-8 text-center">
			<p class="mimic-h2">E-Book</p>
			<p class="mb-5">Here you will find the volunteers’ responses to Roberta’s own words as they engaged with her collection and the things they think people should know about her.</p>
    </div>
    <div class="col-lg-12">
      <div style="position:relative;padding-top:max(60%,326px);height:0;width:100%"><iframe allow="clipboard-write" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=roberta_hewitt&u=nervecentre"></iframe></div>
			<p class="text-center text-small mt-3"><img src="/img/logos/cc.png" alt="Creative Commons Attribution 4.0 International License" width="80px" height="29" class="d-inline-block mr-2"/>This content is licensed under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License.</a></p>
		</div>
	</div>
</div>
<div class="bg-light pb-5">
  <div class="container container-wide position-relative mob-px-4 text-center text-lg-left pb-5 mob-pb-0">
  	<div class="row position-relative z-2 mob-mb-3">
      <div class="col-12 text-center pt-5">
        <p class="mimic-h2">1951-1974 Personal Diary </p>
        <p class="mb-4">You can now read the diary of Roberta Hewitt (D3838/4/2/2) and the transcripts created by our volunteers.</p>
      </div>
    </div>
    <div class="row">
      <projects-transcription-view :pagenumber="{{$pagenumber}}"></projects-transcription-view>
  	</div>
  </div>
  <p class="text-center text-small mt-3"><img src="/img/logos/cc.png" alt="Creative Commons Attribution 4.0 International License" width="80px" height="29" class="d-inline-block mr-2"/>This transcription is licensed under the <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License.</a>*<br/><i>*Scanned archives are not covered by the Creative Commons license.</i></p>
</div>
<div class="container container-wide position-relative z-2">
  @if(count($project->downloads))
	<div class="row">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2">Project Resources</p>
		</div>
		@foreach($project->downloads as $d)
		<div class="col-lg-3">
			<div class="project-resource">
				<div class="icon pt-1">
					@if($d->type == "PDF")
					<img src="/img/icons/pdf.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
					@elseif($d->type == "image")
					<img src="/img/icons/image.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
					@endif
				</div>
				<div class="text pl-3">
					<p class="mb-0"><b>{{$d->title}}</b></p>
					@if($d->type == "PDF")
					<p class="text-purple mb-0"><a href="https://drive.google.com/viewerng/viewer?url={{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="dark"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
					@else
					<p class="mb-0"><a href="{{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="text-purple"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
  @endif
	<div class="row mt-5 pt-5 mob-mt-0">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2">Volunteers</p>
		</div>
		<div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/alexandara-barr.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/alexandara-barr.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/alexandara-barr.png" type="image/png" alt="Alexandra Barr - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Alexandra Barr</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/alison-duddey.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/alison-duddey.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/alison-duddey.png" type="image/png" alt="Alison Duddey - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Alison Duddey</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/anne-bodel.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/anne-bodel.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/anne-bodel.png" type="image/png" alt="Anne Bodel - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Anne Bodel</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/caroline-pollard.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/caroline-pollard.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/caroline-pollard.png" type="image/png" alt="Caroline Pollardy - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Caroline Pollard</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/claire-lowry.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/claire-lowry.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/claire-lowry.png" type="image/png" alt="Claire Lowry - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Claire Lowry</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/frances-mclaughlin.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/frances-mclaughlin.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/frances-mclaughlin.png" type="image/png" alt="Frances McLaughlin - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Frances McLaughlin</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/jennifer-burns.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/jennifer-burns.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/jennifer-burns.png" type="image/png" alt="Jennifer Burns - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Jennifer Burns</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/jennifer-mccrea.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/jennifer-mccrea.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/jennifer-mccrea.png" type="image/png" alt="Jennifer McCrea - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Jennifer McCrea</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/joan-mcgandy.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/joan-mcgandy.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/joan-mcgandy.png" type="image/png" alt="Joan McGandy - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Joan McGandy</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/kelly-mccaughrain.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/kelly-mccaughrain.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/kelly-mccaughrain.png" type="image/png" alt="Kelly McCaughrain - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Kelly McCaughrain</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/nuala-noblett.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/nuala-noblett.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/nuala-noblett.png" type="image/png" alt="Nuala Noblett - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Nuala Noblett</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/susan-taylor.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/susan-taylor.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/susan-taylor.png" type="image/png" alt="Susan Taylor - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Susan Taylor</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/roberta-hewitt/volunteers/tara-mcfadden.webp" type="image/webp"/> 
          <source srcset="/img/projects/roberta-hewitt/volunteers/tara-mcfadden.png" type="image/png"/> 
          <img src="/img/projects/roberta-hewitt/volunteers/tara-mcfadden.png" type="image/png" alt="Tara McFadden - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Tara McFadden</p>
      </div>
    </div>
	</div>
</div>
@endsection