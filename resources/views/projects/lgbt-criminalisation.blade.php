@php
$page = 'Projects';
$pagetitle = $project->title . ' | CollabArchive';
$metadescription = $project->meta_description;
$pagetype = 'light';
$pagename = 'projects';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
/*
.owl-carousel .owl-wrapper {
    display: flex !important;
}
.owl-carousel .owl-item img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    max-width: initial;
}
*/
</style>
@endsection
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 mob-pt-4 mob-px-4">
  <div class="row top-padding">
    <div class="container container-wide">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center pt-5">
        	<p class="text-indigo text-title text-uppercase mb-0"><b>{{$project->category->name}}</b></p>
          <h1 class="mb-3 text-capitalize">{{$project->title}}</h1>
          <p>{{$project->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-pb-0 mob-px-4">
  <div class="img-backdrop my-5">
    <picture>
      <source srcset="{{$project->getFirstMediaUrl('projects','normal-webp')}}" type="image/webp"/> 
      <source srcset="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}"/> 
      <img src="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}" alt="{{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="927" height="573"/>
    </picture>
  </div>
</div>
<div class="container container-wide position-relative mb-5	pb-5 mob-px-4 text-center text-lg-left">
	<div class="row position-relative justify-content-center z-2 mob-mb-3">
		<div class="col-lg-6 text-lg-left mob-px-3 pr-5">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
    			<h2 class="mb-4">About the Project</h2>
    			{!!$project->description!!}
        </div>
      </div>
		</div>
    <div class="col-lg-6">
      <div class="img-backdrop my-5">
        <picture>
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal-webp')}}" type="image/webp"/> 
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}"/> 
          <img src="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}" alt="About the project - {{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="720" height="479"/>
        </picture>
      </div>
    </div>
	</div>
</div>
<div class="bg-light pb-5">
  <div class="container container-wide position-relative mob-px-4 text-center text-lg-left pb-5 mob-pb-0">
    <div class="row position-relative justify-content-center z-2 mob-mb-3">
      <div class="col-12 col-lg-8 text-center pt-5">
        <p class="mimic-h2">NIGRA Scrapbooks (1973-1983)</p>
        <p class="mb-5">See some of the scrapbooks of newspaper cuttings from the Northern Ireland Gay Rights Association collection at PRONI (D3762) that were digitised and indexed by our volunteers.</p>
      </div>
    </div>
    <div class="row">
      <archives-transcription-view :pagenumber="{{$pagenumber}}"></archives-transcription-view>
    </div>
  </div>
</div>
<div class="container pt-5 mob-pt-0">
  <div class="row position-relative justify-content-center z-2 mob-mb-3">
    <div class="col-12 col-lg-8 text-center pt-5">
      <p class="mimic-h2">Zines</p>
      <p class="mb-5">Read the zines that were created by our volunteers documenting today's societal issues such as disability, LGBTQ+ rights, feminism, and activism..</p>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 1: by Catriona</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_8_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 2: by Eilis</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_1_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 3: by Fiona</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_7_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 4: by Irene</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_4_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 5: by Kaavya</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_5_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 6: by Katharina</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_9_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 7: by Kirsty</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_6_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 8: by Martin</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_3_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-5">
      <p class="title">Zine 9: by Tammie</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe allow="clipboard-write" class="embed-responsive-item" sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox allow-forms" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="https://e.issuu.com/embed.html?d=d4844_5_2_2_all_a&u=nervecentre"></iframe>
      </div>
    </div>
    <div class="col-12">
      <p class="text-center text-small mt-3"><img src="/img/logos/cc.png" alt="Creative Commons Attribution 4.0 International License" width="80px" height="29" class="d-inline-block mr-2"/>The above 'zines' are licensed under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License.</a></p>
    </div>
  </div>
</div>
<div class="container container-wide position-relative z-2">
	@if(count($project->downloads))
	<div class="row">
		<div class="col-12 mb-4">
			<p class="mimic-h2">Project Resources</p>
		</div>
		@foreach($project->downloads as $d)
		<div class="col-lg-3">
			<div class="project-resource">
				<div class="icon pt-1">
					@if($d->type == "PDF")
					<img src="/img/icons/pdf.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
					@elseif($d->type == "image")
					<img src="/img/icons/image.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
					@endif
				</div>
				<div class="text pl-3">
					<p class="mb-0"><b>{{$d->title}}</b></p>
					@if($d->type == "PDF")
					<p class="text-purple mb-0"><a href="https://drive.google.com/viewerng/viewer?url={{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="dark"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
					@else
					<p class="mb-0"><a href="{{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="text-purple"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@endif
	<div class="row mt-5 pt-5 mob-mt-0">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2">Volunteers</p>
		</div>
		<div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/katharina-bock.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/katharina-bock.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/katharina-bock.png" type="image/png" alt="Katharina Bock - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Katharina Bock</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/fiona-sheridan.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/fiona-sheridan.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/fiona-sheridan.png" type="image/png" alt="Fiona Sheridan - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Fiona Sheridan</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/kirsty-comac.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/kirsty-comac.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/kirsty-comac.png" type="image/png" alt="Kirsty Comac - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Kirsty Comac</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/eilis-campbell.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/eilis-campbell.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/eilis-campbell.png" type="image/png" alt="Eilís Campbell - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Eilís Campbell</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/tammie-russell.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/tammie-russell.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/tammie-russell.png" type="image/png" alt="Tammie Russell - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Tammie Russell</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/irene-de-la-mora.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/irene-de-la-mora.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/irene-de-la-mora.png" type="image/png" alt="Irene de la Mora - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Irene de la Mora</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/catriona-edington.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/catriona-edington.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/catriona-edington.png" type="image/png" alt="Catriona Edington - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Catriona Edington</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/kaavya-chandrashekar.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/kaavya-chandrashekar.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/kaavya-chandrashekar.png" type="image/png" alt="Kaavya Chandrashekar - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Kaavya Chandrashekar</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/martin-duffy.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/martin-duffy.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/martin-duffy.png" type="image/png" alt="Martin Terence Duffy - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Martin Terence Duffy</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/in-the-archives/emma-mackle.webp" type="image/webp"/> 
          <source srcset="/img/projects/in-the-archives/emma-mackle.png" type="image/png"/> 
          <img src="/img/projects/in-the-archives/emma-mackle.png" type="image/png" alt="Emma Mackle - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Emma Mackle</p>
      </div>
    </div>
	</div>
</div>
@endsection