@php
$page = 'Projects';
$pagetitle = $project->title . ' | CollabArchive';
$metadescription = $project->meta_description;
$pagetype = 'light';
$pagename = 'projects';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
/*
.owl-carousel .owl-wrapper {
    display: flex !important;
}
.owl-carousel .owl-item img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    max-width: initial;
}
*/
</style>
@endsection
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 mob-pt-4 mob-px-4">
  <div class="row top-padding">
    <div class="container container-wide">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center pt-5">
        	<p class="text-pink text-title text-uppercase mb-0"><b>{{$project->category->name}}</b></p>
          <h1 class="mb-3 text-capitalize">{{$project->title}}</h1>
          <p>{{$project->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-pb-0 mob-px-4">
  <div class="img-backdrop my-5">
    <picture>
      <source srcset="{{$project->getFirstMediaUrl('projects','normal-webp')}}" type="image/webp"/> 
      <source srcset="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}"/> 
      <img src="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}" alt="{{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="927" height="573"/>
    </picture>
  </div>
</div>

<div class="container container-wide position-relative mb-5	pb-5 mob-px-4 text-center text-lg-left">
	<div class="row position-relative justify-content-center z-2 mob-mb-3">
		<div class="col-lg-6 text-lg-left mob-px-3 pr-5">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
    			<h2 class="mb-4">About the Project</h2>
    			{!!$project->description!!}
        </div>
      </div>
		</div>
    <div class="col-lg-6">
      <div class="img-backdrop my-5">
        <picture>
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal-webp')}}" type="image/webp"/> 
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}"/> 
          <img src="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}" alt="About the project - {{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="720" height="479"/>
        </picture>
      </div>
    </div>
	</div>
</div>
<div class="container container-wide">
  <div class="row pb-5 mob-pb-0">
    <div class="col-12 text-center text-lg-left mb-4">
      <p class="mimic-h2">Q&A Videos</p>
      <p>Our volunteers interview each other and share stories about music in their lives and what it means to them.</p>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/wbOEP-EC4qo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/TGgIGuJpMy4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/Lyfj5aKMbNc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/oXq_fWEM_q4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/IprmyJpWRr4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/5qqHHVjYSko" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/GPT-XPnkl38" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/nSCNcNFB6jI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/IhVtuRLNYrg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-lg-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9 shadow">
        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/4RbsrNpInwM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-12">
      <p class="text-center text-small mt-3"><img src="/img/logos/cc.png" alt="Creative Commons Attribution 4.0 International License" width="80px" height="29" class="d-inline-block mr-2"/>The above videos are licensed under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License.</a></p>
    </div>
  </div>
</div>
<div class="container container-wide position-relative z-2">
	@if(count($project->downloads))
	<div class="row">
		<div class="col-12 mb-4">
			<p class="mimic-h2">Project Resources</p>
		</div>
		@foreach($project->downloads as $d)
		<div class="col-lg-3">
			<div class="project-resource">
				<div class="icon pt-1">
					@if($d->type == "PDF")
					<img src="/img/icons/pdf.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
					@elseif($d->type == "image")
					<img src="/img/icons/image.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
					@endif
				</div>
				<div class="text pl-3">
					<p class="mb-0"><b>{{$d->title}}</b></p>
					@if($d->type == "PDF")
					<p class="text-purple mb-0"><a href="https://drive.google.com/viewerng/viewer?url={{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="dark"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
					@else
					<p class="mb-0"><a href="{{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="text-purple"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@endif
	<div class="row mt-5 pt-5 mob-mt-0">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2">Volunteers</p>
		</div>
		<div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/stephen-strong.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/stephen-strong.png" type="image/png"/> 
          <img src="/img/projects/music-tales/stephen-strong.png" type="image/png" alt="Stephen Strong - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Stephen Strong</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/mark-mcshane.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/mark-mcshane.png" type="image/png"/> 
          <img src="/img/projects/music-tales/mark-mcshane.png" type="image/png" alt="Mark McShane - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Mark McShane</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/liam-clarke.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/liam-clarke.png" type="image/png"/> 
          <img src="/img/projects/music-tales/liam-clarke.png" type="image/png" alt="Liam Clarke - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Liam Clarke</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/una-otoole.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/una-otoole.png" type="image/png"/> 
          <img src="/img/projects/music-tales/una-otoole.png" type="image/png" alt="Una OTooley - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Una OToole</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/carol-bennett.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/carol-bennett.png" type="image/png"/> 
          <img src="/img/projects/music-tales/carol-bennett.png" type="image/png" alt="Carol Bennett - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Carol Bennett</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/patricia-mcknight.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/patricia-mcknight.png" type="image/png"/> 
          <img src="/img/projects/music-tales/patricia-mcknight.png" type="image/png" alt="Patricia McKnight - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Patricia McKnight</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/anthony-mckeown.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/anthony-mckeown.png" type="image/png"/> 
          <img src="/img/projects/music-tales/anthony-mckeown.png" type="image/png" alt="Anthony McKeown - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Anthony McKeown</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/steff-coyle.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/steff-coyle.png" type="image/png"/> 
          <img src="/img/projects/music-tales/steff-coyle.png" type="image/png" alt="Steff Coyle - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Steff Coyle</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/doris-finley.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/doris-finley.png" type="image/png"/> 
          <img src="/img/projects/music-tales/doris-finley.png" type="image/png" alt="Doris Finley - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Doris Finley</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/music-tales/olive-rodgers.webp" type="image/webp"/> 
          <source srcset="/img/projects/music-tales/olive-rodgers.png" type="image/png"/> 
          <img src="/img/projects/music-tales/olive-rodgers.png" type="image/png" alt="Olive Rodgers - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Olive Rodgers</p>
      </div>
    </div>
	</div>
</div>
@endsection