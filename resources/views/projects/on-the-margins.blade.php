@php
$page = 'Projects';
$pagetitle = $project->title . ' | CollabArchive';
$metadescription = $project->meta_description;
$pagetype = 'light';
$pagename = 'projects';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
/*
.owl-carousel .owl-wrapper {
    display: flex !important;
}
.owl-carousel .owl-item img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    max-width: initial;
}
*/
</style>
@endsection
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 mob-pt-4 mob-px-4">
  <div class="row top-padding">
    <div class="container container-wide">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center pt-5">
        	<p class="text-yellow text-title text-uppercase mb-0"><b>{{$project->category->name}}</b></p>
          <h1 class="mb-3 text-capitalize">{{$project->title}}</h1>
          <p>{{$project->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mob-pb-0 mob-px-4">
  <div class="img-backdrop my-5">
    <picture>
      <source srcset="{{$project->getFirstMediaUrl('projects','normal-webp')}}" type="image/webp"/> 
      <source srcset="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}"/> 
      <img src="{{$project->getFirstMediaUrl('projects','normal')}}" type="{{$project->getFirstMedia('projects')->mime_type}}" alt="{{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="927" height="573"/>
    </picture>
  </div>
</div>
<div class="container container-wide position-relative mb-5	pb-5 mob-px-4 text-center text-lg-left">
	<div class="row position-relative justify-content-center z-2 mob-mb-3">
		<div class="col-lg-6 text-lg-left mob-px-3 pr-5">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
    			<h2 class="mb-4">About the Project</h2>
    			{!!$project->description!!}
        </div>
      </div>
		</div>
    <div class="col-lg-6">
      <div class="img-backdrop my-5">
        <picture>
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal-webp')}}" type="image/webp"/> 
          <source srcset="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}"/> 
          <img src="{{$project->getFirstMediaUrl('aboutimgs','normal')}}" type="{{$project->getFirstMedia('aboutimgs')->mime_type}}" alt="About the project - {{$project->title}} | CollabArchive" class="w-100 h-auto shadow" width="720" height="479"/>
        </picture>
      </div>
    </div>
	</div>
</div>
<div class="container pb-5">
  <div class="row pb-5 mob-pb-0">
    <div class="col-12 text-center text-lg-left">
      <p class="mimic-h2">Asylum Stories & Haikus</p>
      <p>Hear from some of the volunteers about what it was like to follow someone's journey through the archives and to use haikus to bring forward these forgotten stories.</p>
      <div>
        <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1504784917&color=%23d95c92&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/collabarchive" title="CollabArchive" target="_blank" style="color: #cccccc; text-decoration: none;">CollabArchive</a> · <a href="https://soundcloud.com/collabarchive/sets/on-the-margins" title="On the Margins: Stories from Northern Ireland&#x27;s Asylums" target="_blank" style="color: #cccccc; text-decoration: none;">On the Margins: Stories from Northern Ireland&#x27;s Asylums</a>
      </div>
      <p class="text-center text-small mt-3"><img src="/img/logos/cc.png" alt="Creative Commons Attribution 4.0 International License" width="80px" height="29" class="d-inline-block mr-2"/>This content is licensed under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License.</a></p>
    </div>
  </div>
</div>
<div class="container container-wide position-relative z-2">
  @if(count($project->downloads))
  <div class="row">
    <div class="col-12 mb-4">
      <p class="mimic-h2">Project Resources</p>
    </div>
    @foreach($project->downloads as $d)
    <div class="col-lg-3">
      <div class="project-resource">
        <div class="icon pt-1">
          @if($d->type == "PDF")
          <img src="/img/icons/pdf.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
          @elseif($d->type == "image")
          <img src="/img/icons/image.svg" alt="download resource icon" class="h-auto" width="32" height="43"/>
          @endif
        </div>
        <div class="text pl-3">
          <p class="mb-0"><b>{{$d->title}}</b></p>
          @if($d->type == "PDF")
          <p class="text-purple mb-0"><a href="https://drive.google.com/viewerng/viewer?url={{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="dark"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
          @else
          <p class="mb-0"><a href="{{$d->getFirstMediaUrl('downloads')}}" target="_blank" class="text-purple"><b><u>Download</u></b> <i class="fa fa-cloud-download"></i></a></p>
          @endif
        </div>
      </div>
    </div>
    @endforeach
  </div>
  @endif
  <div class="row mt-5 pt-5 mob-mt-0">
    <div class="col-12 mb-4 text-center text-lg-left">
      <p class="mimic-h2">Volunteers</p>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/1-heather-cardwell.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/1-heather-cardwell.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/1-heather-cardwell.png" type="image/png" alt="Heather Cardwell - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Heather Cardwell</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/2-alison-walsh.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/2-alison-walsh.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/2-alison-walsh.png" type="image/png" alt="Alison Walsh - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Alison Walsh</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/3-stephanie-carruthers.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/3-stephanie-carruthers.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/3-stephanie-carruthers.png" type="image/png" alt="Stephanie Carruthers - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Stephanie Carruthers</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/4-john-robinson.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/4-john-robinson.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/4-john-robinson.png" type="image/png" alt="John Robinson - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">John Robinson</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/5-jane-robinson.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/5-jane-robinson.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/5-jane-robinson.png" type="image/png" alt="Jane Robinson - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Jane Robinson</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/6-joy-entwistle.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/6-joy-entwistle.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/6-joy-entwistle.png" type="image/png" alt="Joy Entwistle - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Joy Entwistle</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/7-julie-mcGrath.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/7-julie-mcGrath.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/7-julie-mcGrath.png" type="image/png" alt="Julie McGrath - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Julie McGrath</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/8-geraldine-mccrossan.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/8-geraldine-mccrossan.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/8-geraldine-mccrossan.png" type="image/png" alt="Geraldine McCrossan - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Geraldine McCrossan</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/9-shaun-hickland.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/9-shaun-hickland.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/9-shaun-hickland.png" type="image/png" alt="Shaun Hickland - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Shaun Hickland</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/10-melissa-johnston.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/10-melissa-johnston.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/10-melissa-johnston.png" type="image/png" alt="Melissa Johnston - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Melissa Johnston</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/11-ciara-barkley.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/11-ciara-barkley.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/11-ciara-barkley.png" type="image/png" alt="Ciara Barkley - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Ciara Barkley</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/12-joanne-glasgow.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/12-joanne-glasgow.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/12-joanne-glasgow.png" type="image/png" alt="Joanne Glasgow - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Joanne Glasgow</p>
      </div>
    </div>
    <div class="col-lg-3 mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/projects/on-the-margins/13-liz-cullinane.webp" type="image/webp"/> 
          <source srcset="/img/projects/on-the-margins/13-liz-cullinane.png" type="image/png"/> 
          <img src="/img/projects/on-the-margins/13-liz-cullinane.png" type="image/png" alt="Liz Cullinane - Volunteer CollabArchive" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0">Liz Cullinane</p>
      </div>
    </div>

  </div>
</div>
@endsection