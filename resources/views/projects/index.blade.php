@php
$page = 'Projects';
$pagetitle = 'Projects | CollabArchive';
$metadescription = 'Learn more about each of the five engagement and volunteering projects we will deliver during 2022.';
$pagetype = 'light';
$pagename = 'projects';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-1">
  <div class="row top-padding">
    <div class="container container-wide">
      <div class="row justify-content-center">
        <div class="col-12 text-center text-lg-left">
          <h1 class="pt-5 mb-3 mob-mt-4 text-capitalize">Projects</h1>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<div class="row position-relative z-2 mob-mb-3">
		<projects-index :categories="{{$categories}}"></projects-index>
	</div>
</div>
@endsection