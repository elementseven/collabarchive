@php
$page = 'Blog';
$pagetitle = 'CollabArchive Blog - Stay up to date';
$metadescription = 'See what the CollabArchive team and volunteers are up to.';
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://collabarchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 pt-4">
  <div class="row top-padding">
    <div class="container container-wide">
      <div class="row justify-content-center">
        <div class="col-12 text-center text-lg-left">
          <h1 class="pt-5 mb-5 mob-mb-3 text-capitalize">Blog Posts</h1>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<div class="row position-relative z-2 mob-mb-3">
		<blog-index :categories="{{$categories}}"></blog-index>
	</div>
</div>
@endsection