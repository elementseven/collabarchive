@php
$page = 'Blog';
$pagetitle = $post->title . ' | CollabArchive';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'light';
$pagename = 'blog';
$ogimage = 'https://collabarchive.org' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('styles')
<style>
  #app{
    overflow: visible !important;
  }
  .make-me-sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 120px;
  }
  .line-white{
    border-bottom: 1px solid #fff;
  }
</style>
@endsection
@section('header')
<header id="page-header" class="container container-wide position-relative z-2 blog-header bg top-padding">
  <div class="row pt-5">
    <div class="col-lg-6 py-5 mob-pb-0 pr-5 mob-px-3 text-center text-lg-left">
      <div class="pr-5 mob-px-0">
        <p class="cursor-pointer text-title text-purple text-uppercase mb-0"><b>{{\Carbon\Carbon::parse($post->created_at)->format('jS M Y')}}</b></p>
        <h1 class="mb-3 blog-title">{{$post->title}}</h1>
        <p class="text-large mb-4">{{$post->excerpt}}</p>
        <p class="mb-3"><b>Share:</b>
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
            <i class="fa fa-facebook ml-3 "></i>
          </a>
          <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
            <i class="fa fa-linkedin ml-3"></i>
          </a>
          <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
            <i class="fa fa-twitter ml-3"></i>
          </a>
          <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
            <i class="fa fa-whatsapp ml-3"></i>
          </a>
        </p>
      </div>
    </div>
    <div class="col-lg-6 py-5 mob-pb-0">
      <div class="img-backdrop">
        <picture>
          <source srcset="{{$post->getFirstMediaUrl('blog','double-webp')}}" type="image/webp"/> 
          <source srcset="{{$post->getFirstMediaUrl('blog','double')}}" type="{{$post->mime_type}}"/> 
          <img src="{{$post->getFirstMediaUrl('blog','double')}}" type="image/jpeg" alt="Public Records Office Northern Ireland Belfast" class="w-100 h-auto shadow" width="927" height="573"/>
        </picture>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<article>
  <div class="container container-wide mob-px-4 position-relative z-2">
    <div class="row mob-py-0 text-center text-lg-left">
      <div class="col-lg-9 my-5 mob-mb-3 pr-5 mob-px-3 ipadp-px-3">
        <div class="blog-body">
          {!!$post->content!!}
           <p class="mt-4 mb-0 mob-mb-5"><b>Share:</b>  
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook ml-3 "></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
      <div class="col-lg-3 py-5 mob-pt-0">
        <div class="make-me-sticky">
          <div class="row">
            <div class="col-12">
              <p class="mimic-h3 mb-0">More Blog Posts</p>
            </div>
            @foreach($others as $article)
            <div class="col-12 col-md-6 col-lg-12 mt-4">
              <a href="/blog/{{\Carbon\Carbon::parse($article->created_at)->format('Y-m-d')}}/{{$article->slug}}">
                <div class="card bg-white boder-0 shadow text-center p-4">
                  <p class="text-small cursor-pointer text-title text-purple text-uppercase mb-0"><b>{{\Carbon\Carbon::parse($article->created_at)->format('jS M Y')}}</b></p>
                  <p class="text-large text-title text-dark mb-0 cursor-pointer">{{$article->title}}</p>
                  <p class="text-one text-dark mb-2">{{substr($article->excerpt, 0, 100)}}</p>
                  <p class="text-primary text-title text-uppercase mb-0"><u>Read article</u> <i class="fa fa-angle-double-right cursor-pointer"></i></p>
                </div>
              </a>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</article>
@endsection
@section('scripts')
<style>
  .fr-video{
    display: block;
    overflow: hidden;
    padding: 0;
    position: relative;
    width: 100%;
    background-color: #000;
  }
  .fr-video:before {
    padding-top: 56.25%;
    content: "";
    display: block;
  }
  .fr-video.soundcloud: before{
    padding-top: 150px !important;
  }
  .fr-video iframe,
  .fr-video video{
    border: 0;
    bottom: 0;
    height: 100% !important;
    left: 0;
    position: absolute;
    top: 0;
    width: 100% !important;
  }
  .fr-img-caption .fr-inner{
    font-size: 16px;
    margin-top: 6px;
    margin-bottom: 1rem !important;
    display: block;
    color: #666;
  }
  .fr-video.soundcloud:before{
    padding-top: 150px;
  }
</style>
<script>
  $(window).on('load', function () {
    $('.fr-video iframe').each(function(i, e){
      var src = e.src;
      var url = "soundcloud";
      if(src.includes(url)){
        $(e.parentElement).addClass("soundcloud");
      }else {
        var width = $(e).width();
        $(e).css({
          "width": width,
          "height": width*(9/16)
        });
      }
    });
  });
</script>
@endsection