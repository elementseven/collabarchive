@php
$page = 'Homepage';
$pagetitle = "CollabArchive | Connecting new volunteers with archives through creativity & digital technology.";
$metadescription = "CollabArchive is a digital volunteering project led by the Nerve Centre and the Public Record Office of Northern Ireland (PRONI) and funded by the National Lottery Heritage Fund.";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://collabarhchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-1">
  <div class="row top-padding">
    <div class="container pt-4">
      <div class="row justify-content-center">
        <div class="col-lg-10 text-center">
          <h1 class="pt-5 mb-4 mob-mb-3 text-capitalize">Connecting new volunteers<br class="d-none d-lg-inline" /> with archives through creativity & digital technology.</h1>
          <p class="text-large mb-4">CollabArchive is a digital volunteering project led by the Nerve Centre and the Public Record Office of Northern Ireland (PRONI) and funded by the National Lottery Heritage Fund.</p>
          <a href="/projects">
            <button class="btn btn-primary" type="button" data-aos="fade-up">Browse Projects</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container py-5">
  <div class="img-backdrop my-5 mob-my-3">
    <picture>
      <source srcset="/img/home/home-1.webp" type="image/webp"/> 
      <source srcset="/img/home/home-1.jpg" type="image/jpeg"/> 
      <img src="/img/home/home-1.jpg" type="image/jpeg" alt="CollabArchive Volunteers Enjoying their work" class="w-100 h-auto shadow" width="927" height="573"/>
    </picture>
  </div>
</div>
<div class="container-fluid text-center text-lg-left mob-px-4">
  <div class="row">
    <div class="col-lg-6 text-right pr-5 d-none d-lg-block">
      <img src="/img/graphics/big-c.svg" alt="CollabArchive big C graphic" class="h-auto" width="597" height="707" />
    </div>
    <div class="col-lg-6 position-relative">
      <div class="mob-big-c-bg"></div>
      <div class="d-table w-100 h-100" style="max-width:670px;">
        <div class="d-table-cell align-middle w-100 h-100">
          <h2 class="mb-3">What is<br/>CollabArchive?</h2>
          <p class="text-large mb-4">CollabArchive is a digital volunteering project from the Nerve Centre, in partnership with the Public Record Office of Northern Ireland (PRONI) that will create unique digital volunteering opportunities for the public to engage with archives throughout 2022. The CollabArchive project is funded by the National Lottery Heritage Fund. </p>
          <a href="/about">
            <button class="btn btn-primary" type="button" data-aos="fade-up" >Learn More</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container container-wide py-5 text-center text-lg-left mob-px-4">
  <div class="row pt-5">
    <div class="col-lg-8">
      <h2 class="mb-3">Projects</h2>
      <p class="text-large">Over the course of 2022, we will open PRONI’s archives to explore five themes, including women's history, migration, mental health, LGBT history and music. Volunteers will produce creative outputs as a response to their engagement with the archives and will also help PRONI make their collections more digitally accessible to the public.</p>
    </div>
  </div>
  <projects-inline :count="5"></projects-inline>
</div>
<div class="container container-wide pb-5 mob-pt-5 mob-pb-0">
  <div class="row">
    <div class="col-lg-12 text-center">
      <h2 class="mb-3">Read Our Blog</h2>
      <p class="text-large">See what the CollabArchive team and volunteers are up to.</p>
    </div>
    <blog-home></blog-home>
    <div class="col-12 text-center mob-mt-4">
      <p class="mimic-h3 mb-2">More Blog Articles?</p>
      <p>Check out all of our blog articles.</p>
      <a href="/blog">
        <button type="button" class="btn btn-primary" data-aos="fade-up">Browse Blog</button>
      </a>
    </div>
  </div>
</div>
@endsection
@section('scripts')

@endsection