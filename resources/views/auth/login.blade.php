@php
$page = 'Login';
$pagename = 'Login';
$pagetitle = "Login - CollabArchive user area";
$pagetype = 'light';
$metadescription = 'Log in to your CollabArchive account!';
$ogimage = "https://collabarchive.org/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'pagetype' => $pagetype, 'metadescription' => $metadescription , 'ogimage' => $ogimage, 'pagename' => $pagename])
@section('content')
<div class="container py-5 mt-5">
    <div class="row justify-content-center  top-padding">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body px-3 py-5">
                    <h1 class="text-center mb-4">Login</h1>
{{--                     <p class="text-center">User area coming soon.</p>
 --}}                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        @if (isset($_GET['redirect']))
                        @php Session()->put('url.intended', url('/' . $_GET['redirect'])); @endphp
                        @endif
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row text-center">
                            <div class="col-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" checked>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0 text-center">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary d-inline-block">
                                    {{ __('Login') }}
                                </button>
                                <br/>
                                @if (Route::has('password.request'))
                                <a class="d-inline-block mt-2 " href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection