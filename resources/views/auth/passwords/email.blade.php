@php
$page = 'Forgot Password';
$pagename = 'Forgot Password';
$pagetitle = "Forgot Password - CollabArchive user area";
$pagetype = 'light';
$metadescription = 'Forgot your CollabArchive Password?';
$ogimage = "https://collabarchive.org/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'pagetype' => $pagetype, 'metadescription' => $metadescription , 'ogimage' => $ogimage, 'pagename' => $pagename])
@section('content')
<div class="container mt-5 py-5">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
            <div class="card p-4 mt-5 mob-mt-0">
                <div class="card-body text-center">
                    <h1 class="text-center blog-title mb-2">Forgot Password?</h1>
                    <p class="">If you have forgotten your password, enter your email address below and we will send you a password reset link.</p>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" class="row" action="{{ route('password.email') }}">
                        @csrf

                        <div class="col-12 mb-3 text-left">
                            <label for="email" class="text-left"><b>{{ __('Email Address') }}</b></label>

                 
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email address">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                         
                        </div>

                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary mx-auto d-inline-block">
                                    {{ __('Send Reset Link') }}
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
