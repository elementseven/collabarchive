@php
$page = 'Reset Password';
$pagename = 'Reset Password';
$pagetitle = "Reset Password - CollabArchive user area";
$pagetype = 'light';
$metadescription = 'Reset your CollabArchive Password?';
$ogimage = "https://collabarchive.org/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'pagetype' => $pagetype, 'metadescription' => $metadescription , 'ogimage' => $ogimage, 'pagename' => $pagename])
@section('content')
<div class="container mt-5 py-5">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-5">
            <div class="card p-4 mt-5 mob-mt-0">
                <div class="card-body text-center">
                    <h1 class="text-center blog-title mb-3">Reset Password</h1>
                    <form method="POST" class="row" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="col-12 mb-3 text-left">
                            <label for="email" class="text-md-end"><b>{{ __('Email Address') }}</b></label>

                    
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                         
                        </div>

                        <div class="col-12 mb-3 text-left">
                            <label for="password" class="text-md-end"><b>{{ __('Password') }}</b></label>

                 
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter a new password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                 
                        </div>

                        <div class="col-12 mb-4 text-left">
                            <label for="password-confirm" class="text-md-end"><b>{{ __('Confirm Password') }}</b></label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm your new password">
                          
                        </div>

                        <div class="col-12">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
