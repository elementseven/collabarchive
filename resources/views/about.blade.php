@php
$page = 'About';
$pagetitle = "About | CollabArchive";
$metadescription = "CollabArchive is a digital volunteering project from the Nerve Centre, in partnership with the Public Record Office of Northern Ireland (PRONI), that will create unique digital volunteering opportunities for the public to engage with archives. The CollabArchive project is funded by the National Lottery Heritage Fund.";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://collabarhchive.org/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header id="page-header" class="container-fluid position-relative z-1 mob-pt-5 pt-4 mob-px-4">
  <div class="row top-padding">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-9 px-0 mob-px-3 text-center">
          <h1 class="pt-5 mb-4 text-capitalize">About CollabArchive</h1>
          <p class="text-large mb-4 mob-mb-0">CollabArchive is a digital volunteering project from the Nerve Centre, in partnership with the Public Record Office of Northern Ireland (PRONI), that will create unique digital volunteering opportunities for the public to engage with archives. The CollabArchive project is funded by the National Lottery Heritage Fund.</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container container-wide pb-5 mob-pb-0 mob-px-4 text-center text-lg-left">
  <div class="row py-5 mob-pt-3">
    <div class="col-lg-6">
      <div class="img-backdrop mt-5">
        <carousel :items="1" :margin="15" :center="false"  :autoHeight="true" :autoplay="true" :infinite="true" :autoWidth="true" :loop="true" :autoplayTimeout="6000" :nav="false" :dots="false">
          <div>
            <picture>
              <source srcset="/img/about/the-project.webp" type="image/webp"/> 
              <source srcset="/img/about/the-project.jpg" type="image/jpeg"/> 
              <img src="/img/about/the-project.jpg" type="image/jpeg" alt="The CollabArchive Project Volunteers" class="w-100 h-auto shadow" width="927" height="573"/>
            </picture>
          </div>
          <div>
            <picture>
              <source srcset="/img/about/the-project-2.webp" type="image/webp"/> 
              <source srcset="/img/about/the-project-2.jpg" type="image/jpeg"/> 
              <img src="/img/about/the-project-2.jpg" type="image/jpeg" alt="The CollabArchive Project Volunteers" class="w-100 h-auto shadow" width="927" height="573"/>
            </picture>
          </div>
          <div>
            <picture>
              <source srcset="/img/about/the-project-3.webp" type="image/webp"/> 
              <source srcset="/img/about/the-project-3.jpg" type="image/jpeg"/> 
              <img src="/img/about/the-project-3.jpg" type="image/jpeg" alt="The CollabArchive Project Volunteers" class="w-100 h-auto shadow" width="927" height="573"/>
            </picture>
          </div>
        </carousel>
      </div>
    </div>
    <div class="col-lg-6 pl-5 pt-5 mob-px-3 mob-pt-3">
      <h2 class="mb-3 mt-5">The Project</h2>
      <p>Throughout 2022, the skills-based project will develop volunteering opportunities for underrepresented audiences, extending and enhancing the value of archives to those who traditionally have not engaged with them.</p>

      <p>Volunteer participants will be at the heart of the project, bringing forward archive collections most important to them, and using digital media, a website and online platforms such as PRONI and this website to showcase their work.</p>

      <p>The aim of the project is to develop a sustainable model of digital volunteering engagement that supports new, non-traditional participation with the archives.</p>

    </div>
  </div>
  <div class="row py-5 mob-py-0">
    <div class="col-12 d-lg-none">
      <div class="img-backdrop">
        <picture>
          <source srcset="/img/about/proni.webp" type="image/webp"/> 
          <source srcset="/img/about/proni.jpg" type="image/jpeg"/> 
          <img src="/img/about/proni.jpg" type="image/jpeg" alt="Public Records Office Northern Ireland Belfast" class="w-100 h-auto shadow" width="927" height="573"/>
        </picture>
      </div>
    </div>
    <div class="col-lg-6 pr-5 pt-5 mob-px-3">
      <h2 class="mb-3">About PRONI</h2>
      <p>The Public Record Office of Northern Ireland (PRONI) was established in 1923 and is the official archive for Northern Ireland. PRONI operates as a directorate within the Engaged Communities Group as part of the Department for Communities (DfC). PRONI aims to identify, preserve and make available Northern Ireland’s unique archival heritage.</p>
      <p>PRONI holds over 3.5 million documents that relate chiefly, but not exclusively, to Northern Ireland. They date largely from c.1600 to the present day (with a few dating back as far as the early 13th century).PRONI holds the records of government departments and non-departmental public bodies. It also holds privately deposited archives from a range of sources including churches, businesses, private individuals and families.</p>
    </div>
    <div class="col-lg-6 d-none d-lg-block">
      <div class="img-backdrop mt-5">
        <picture>
          <source srcset="/img/about/proni.webp" type="image/webp"/> 
          <source srcset="/img/about/proni.jpg" type="image/jpeg"/> 
          <img src="/img/about/proni.jpg" type="image/jpeg" alt="Public Records Office Northern Ireland Belfast" class="w-100 h-auto shadow" width="927" height="573"/>
        </picture>
      </div>
    </div>
  </div>
  <div class="row pt-5 mob-pt-0">
    <div class="col-lg-6">
      <div class="img-backdrop mt-5">
        <picture>
          <source srcset="/img/about/nerve-centre.webp" type="image/webp"/> 
          <source srcset="/img/about/nerve-centre.jpg" type="image/jpeg"/> 
          <img src="/img/about/nerve-centre.jpg" type="image/jpeg" alt="The Nerve Centre Londonderry" class="w-100 h-auto shadow" width="927" height="573"/>
        </picture>
      </div>
    </div>
    <div class="col-lg-6 pl-5 pt-5 mob-px-3">
      <h2 class="mb-3">About Nerve Centre</h2>
      <p>Nerve Centre is Northern Ireland’s leading creative media arts centre with a vision of ‘changing lives through creative technologies and the arts’. More than 120,000 people a year benefit from a wide-ranging programme of arts events, innovative projects, creative learning, and production facilities. </p>
      <p>A- successful social enterprise, the Nerve Centre employs 50 staff in Derry~Londonderry and Belfast. At an educational level, the Nerve Centre has developed the Creative Learning Centre model, empowering teachers and community learners to engage with creative technologies to unlock learning in the curriculum. The Nerve Centre’s artistic output has gained an international reputation with Oscar and BAFTA nominations, and an ongoing collaboration with visual artist Willie Doherty, twice nominated for the Turner Prize.</p>
    </div>
  </div>
</div>
<div class="container pb-5 pt-4">
  <div class="row justify-content-center pt-5">
    <div class="col-12 text-center mb-4">
      <h2>CollabArchive Team</h2>
    </div>
    <div class="col-lg-5 mob-mb-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/team/laura-aguiar.webp" type="image/webp"/> 
          <source srcset="/img/team/laura-aguiar.png" type="image/png"/> 
          <img src="/img/team/laura-aguiar.png" type="image/png" alt="Laura Aguiar - Community Engagement Officer & Creative Producer" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0"><b>LAURA AGUIAR</b></p>
        <p class="">Community Engagement Officer<br/>& Creative Producer</p>
        <p class="mb-0 text-large"><a href="https://twitter.com/lauraslaguiar" target="_blank"><i class="fa fa-twitter ml-3"></i></a></p>
      </div>
    </div>
    <div class="col-lg-5">
      <div class="card team-card border-0 bg-white text-center">
        <picture>
          <source srcset="/img/team/niall-kerr.webp" type="image/webp"/> 
          <source srcset="/img/team/niall-kerr.png" type="image/png"/> 
          <img src="/img/team/niall-kerr.png" type="image/png" alt="Niall Kerr - Project Manager" class="h-auto mb-2" width="200" height="200"/>
        </picture>
        <p class="text-title text-larger mb-0"><b>NIALL KERR</b></p>
        <p class="mb-5 mob-mb-3">Project Manager</p>
        <p class="mb-0 text-large"><a href="https://twitter.com/nia_ker" target="_blank"><i class="fa fa-twitter ml-3"></i></a></p>
      </div>
    </div>
  </div>
</div>
@endsection