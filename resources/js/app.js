/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("babel-polyfill");
require("whatwg-fetch");
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
import Vue from 'vue';

require('./plugins/modernizr-custom.js'); 
require('waypoints/lib/jquery.waypoints.min.js');
require('lite-youtube-embed/src/lite-yt-embed.js');
require('./plugins/cookieConsent.js');

window.AOS = require('aos');
AOS.init();
window.addEventListener('load', AOS.refresh);
window.addEventListener('resize', AOS.refresh);


import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import { Datetime } from 'vue-datetime';
Vue.component('datetime', Datetime);

import vueDebounce from 'vue-debounce';
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup']
});

import carousel from 'vue-owl-carousel';


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('main-menu', () => import(/* webpackChunkName: "MainMenu" */ './components/Menus/MainMenu.vue'));
Vue.component('mobile-menu', () => import(/* webpackChunkName: "MobileMenu" */ './components/Menus/MobileMenu.vue'));
Vue.component('site-footer', () => import(/* webpackChunkName: "Footer" */ './components/Footer.vue'));

Vue.component('mailing-list', () => import(/* webpackChunkName: "MailingList" */ './components/MailingList.vue'));
Vue.component('loader', () => import(/* webpackChunkName: "Loader" */ './components/Loader.vue'));

Vue.component('contact-page-form', () => import(/* webpackChunkName: "ContactPageForm" */ './components/Contact/ContactPageForm.vue'));

Vue.component('blog-index', () => import(/* webpackChunkName: "BlogIndex" */ './components/blog/Index.vue'));
Vue.component('blog-show', () => import(/* webpackChunkName: "BlogShow" */ './components/blog/Show.vue'));
Vue.component('blog-home', () => import(/* webpackChunkName: "BlogHome" */ './components/blog/Home.vue'));

Vue.component('projects-index', () => import(/* webpackChunkName: "ProjectsIndex" */ './components/Projects/Index.vue'));
Vue.component('projects-show', () => import(/* webpackChunkName: "ProjectsShow" */ './components/Projects/Show.vue'));
Vue.component('projects-inline', () => import(/* webpackChunkName: "ProjectsInline" */ './components/Projects/Inline.vue'));
Vue.component('projects-transcription-view', () => import(/* webpackChunkName: "TranscriptionView" */ './components/Projects/TranscriptionView.vue'));
Vue.component('lca-transcription-view', () => import(/* webpackChunkName: "LCATranscriptionView" */ './components/Projects/LCATranscriptionView.vue'));
Vue.component('archives-transcription-view', () => import(/* webpackChunkName: "ArchivesTranscriptionView" */ './components/Projects/ArchivesTranscriptionView.vue'));

Vue.component('carousel', () => import(/* webpackChunkName: "Carousel" */ 'vue-owl-carousel'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
