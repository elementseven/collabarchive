/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('jquery');

import Vuetify from 'vuetify'; 
import "babel-polyfill";
import VueMeta from 'vue-meta';
import VueRouter from 'vue-router';
import App from './users/views/App';
import ZoomOnHover from "vue-zoom-on-hover";
import Vue from 'vue';

window.moment = require('moment');
Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
});

Vue.use(ZoomOnHover);

Vue.component('laravel-pagination', require('laravel-vue-pagination'));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
          path: '/users/dashboard',
          name: 'Dashboard',
          component:
              () => import(/* webpackChunkName: "UsersDashboard" */ './users/views/Dashboard.vue'),
        },
        {
          path: '/users/account',
          name: 'Account',
          component:
              () => import(/* webpackChunkName: "UsersAccount" */ './users/views/Account.vue'),
        },
        {
          path: '/users/projects',
          name: 'Projects',
          component:
              () => import(/* webpackChunkName: "UsersProjects" */ './users/views/Projects.vue'),
        },
        {
          path: '/users/projects/:projectSlug',
          name: 'Project',
          component:
              () => import(/* webpackChunkName: "UsersProject" */ './users/views/Project.vue'),
        },
        {
          path: '/users/tasks/:taskId/:taskSlug',
          name: 'Task',
          component:
              () => import(/* webpackChunkName: "UserTask" */ './users/views/Task.vue'),
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    vuetify: new Vuetify({
      theme: {
        themes: {
          light: {
            primary: '#D95C92',
            secondary: '#5C478F',
            green: '#50EA9D',
            accent: '#D95C92',
            error: '#b71c1c',
          },
        },
      },
    }),
});