let mix = require('laravel-mix');
require('laravel-mix-purgecss');
const TargetsPlugin = require('targets-webpack-plugin');
require('laravel-mix-merge-manifest');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
  postCss: [
    require('autoprefixer'),
  ],
});

mix.babelConfig({
  plugins: ['@babel/plugin-syntax-dynamic-import']
});

mix.webpackConfig({
    plugins: [
    new TargetsPlugin({
      browsers: ['last 2 versions', 'chrome >= 41', 'IE 11'],
    }),
  ],
  output: {
    chunkFilename: 'js/[name].js?id=[chunkhash]',
  },
});

mix.js('resources/js/users.js', 'public/users/js')
    .vue()
    .extract()
    .sass('resources/sass/users.scss', 'public/users/css', {
      implementation: require('node-sass')
    })
    .minify('public/users/css/app.css')
    .version();

mix.mergeManifest();