<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Category;
use App\Models\Project;
use App\Models\Task;

use Illuminate\Http\Request;
use Auth;

class ProjectController extends Controller
{

    /**
     * Get tasks for front end
     *
     * @return \Illuminate\Http\Response
     */
    public function getTasks(Request $request, Assignment $assignment)
    {
        $tasks = Task::where('assignment_id', $assignment->id)
            ->where('status','Complete')
            ->orderBy('order','asc')
            ->paginate($request->input('limit'));

        foreach($tasks as $p){
          $p->normal = $p->getFirstMediaUrl('tasks', 'normal');
          $p->normalwebp = $p->getFirstMediaUrl('tasks', 'normal-webp');
          $p->mimetype = $p->getFirstMedia('tasks')->mime_type;
        }
        if($assignment->id == 1){
            $request->session()->forget('pagenumber');
            $request->session()->push('pagenumber', $request['page']);
        }else if($assignment->id == 7){
            $request->session()->forget('lca_pagenumber');
            $request->session()->push('lca_pagenumber', $request['page']);
        }else if($assignment->id == 9){
            $request->session()->forget('archives_pagenumber');
            $request->session()->push('archives_pagenumber', $request['page']);
        }
        

        return $tasks;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::has('projects')->orderBy('name','asc')->get();
        return view('projects.index')->with(['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        if($request->input('category') == '*'){
          $posts = Project::where('status','!=','draft')
          ->orderBy('id','asc')
          ->with('category')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at','category_id']);
        }else{
          $posts = Project::whereHas('category', function($q) use($request){
              $q->where('id', $request->input('category'));
          })
          ->where('status','!=','draft')
          ->orderBy('id','asc')
          ->with('category')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at','category_id']);
        }
        foreach($posts as $p){
          $p->normal = $p->getFirstMediaUrl('projects', 'normal');
          $p->normalwebp = $p->getFirstMediaUrl('projects', 'normal-webp');
          $p->double = $p->getFirstMediaUrl('projects', 'double');
          $p->doublewebp = $p->getFirstMediaUrl('projects', 'double-webp');
          $p->featured = $p->getFirstMediaUrl('projects', 'featured');
          $p->featuredwebp = $p->getFirstMediaUrl('projects', 'featured-webp');
          $p->mimetype = $p->getFirstMedia('projects')->mime_type;
        }
        return $posts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $project = Project::where('slug', $slug)->with('category')->first();

        if($project->id == 1){
            if ($request->session()->exists('pagenumber')) {
                $pagenumber = $request->session()->get('pagenumber');
            }else{
                $pagenumber = [1];
            }
            return view('projects.roberta-hewitt')->with(['project' => $project,'pagenumber' => $pagenumber[0]]);
        }else if($project->id == 2){
            if ($request->session()->exists('lca_pagenumber')) {
                $pagenumber = $request->session()->get('lca_pagenumber');
            }else{
                $pagenumber = [1];
            }
            return view('projects.leaving-crossing-arriving')->with(['project' => $project,'pagenumber' => $pagenumber[0]]);
        }else if($project->id == 3){
            return view('projects.music-tales')->with(['project' => $project]);
        }else if($project->id == 4){
            return view('projects.on-the-margins')->with(['project' => $project]);
        }else if($project->id == 5){
            if ($request->session()->exists('archives_pagenumber')) {
                $pagenumber = $request->session()->get('archives_pagenumber');
            }else{
                $pagenumber = [1];
            }
            return view('projects.lgbt-criminalisation')->with(['project' => $project,'pagenumber' => $pagenumber[0]]);
        }
    }

    /**
     * Get all of tjhe projects a user is involved in.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function usersProjects(Request $request)
    {
        $user = Auth::user();
        $tasks = $user->tasks;
        $projects = array();

        foreach($tasks as $task){
            $project = $task->assignment->project;
            if(!in_array($project, $projects)){
                array_push($projects, $project);
            }
        }
        foreach($projects as $p){
            $usertaskscount = 0;
            $usertaskscomplete = 0;
            foreach($p->assignments as $a){
                foreach($a->tasks as $t){
                    if($t->user_id == $user->id){
                        $usertaskscount++;
                    }
                    if($t->status == "Submitted" || $t->status == "Complete"){
                        $usertaskscomplete++;
                    }
                }
            }
            $p->usertaskscount = $usertaskscount;
            $p->usertaskscomplete = $usertaskscomplete;
        }
        
        return $projects;

    }

    /**
     * Get a specific project and related user's tasks.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function usersProjectGet($slug){
        $user = Auth::user();
        $project = Project::where('slug', $slug)->with(['category','downloads'])->first();
        $tasks = array();
        $usertaskcount = 0;
        $usertaskscomplete = 0;
        foreach($project->assignments as $a){
            foreach($a->tasks as $t){
                if($t->user_id == $user->id){
                    array_push($tasks, $t);
                    $usertaskcount++;
                }
                if($t->status == "Submitted" || $t->status == "Complete"){
                    $usertaskscomplete++;
                }
            }
        }

        foreach($project->downloads as $d){
            $d->link = $d->getFirstMediaUrl('downloads');
        }

        $project->usertaskscount = $usertaskcount;
        $project->usertaskscomplete = $usertaskscomplete;
        $project->usertasks = $tasks;

        return $project;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
