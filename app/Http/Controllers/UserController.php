<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;

class UserController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = Auth::user();
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $user->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone')
        ]);
        return response()->json(['success' => 'success', 200]);
    }

    /**
   * Update the users password.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
    public function updatePassword(Request $request){
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $updated = array(
                'password' => bcrypt($request->input('password'))
            );
            $user->update($updated);
            return back();
        }
        return back();
    }

    // Function to check password
    private function check($password, $check){
        if(Hash::check($check, $password)) {
            return true;
        } else {
            return false;
        }
    }
}
