<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogCategory;
use App\Models\Post;

class BlogController extends Controller
{
    // Main blog page
    public function index(){
        $categories = BlogCategory::has('posts')->orderBy('name','asc')->get();
        return view('blog.index')->with(['categories' => $categories]);
    }

    // Return json blog posts
    public function get(Request $request){
        $search = $request->input('search');
        if($request->input('category') == '*'){
          $posts = Post::where('status','!=','draft')
          ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
          })
          ->orderBy('created_at','desc')
          ->with('blogCategories')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
        }else{
          $posts = Post::orderBy('created_at','desc')->whereHas('blogCategories', function($q) use($request){
              $q->where('id', $request->input('category'));
          })
          ->when($search != "", function($q) use ($search) {
            $q->where('title','LIKE', '%'.$search.'%');
          })
          ->where('status','!=','draft')
          ->orderBy('created_at','desc')
          ->with('blogCategories')
          ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
        }
        foreach($posts as $p){
          $p->normal = $p->getFirstMediaUrl('blog', 'normal');
          $p->normalwebp = $p->getFirstMediaUrl('blog', 'normal-webp');
          $p->double = $p->getFirstMediaUrl('blog', 'double');
          $p->doublewebp = $p->getFirstMediaUrl('blog', 'double-webp');
          $p->featured = $p->getFirstMediaUrl('blog', 'featured');
          $p->featuredwebp = $p->getFirstMediaUrl('blog', 'featured-webp');
          $p->mimetype = $p->getFirstMedia('blog')->mime_type;
        }
        return $posts;
    }
    
    // Single blog article
    public function show($date, $slug){
        $post = Post::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
        $others = Post::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(2);
        return view('blog.show')->with(['post' => $post, 'others' => $others]);
    }
}
