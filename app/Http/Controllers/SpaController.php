<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SpaController extends Controller
{
  public function users()
  {
    $user = Auth::user();
    return view('users')->with('user', $user);
  }
}