<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Models\User;

class SendMail extends Controller
{
    public function enquiry(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "General Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        Mail::send('emails.enquiry',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $phone){
            $message->from('donotreply@collabarchive.org', 'CollabArchive');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('l.aguiar@nervecentre.org');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }
}
