<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Task extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'slug',
        'status',
        'type',
        'instructions',
        'content',
        'video_link',
        'order',
        'file',
        'assignment_id',
        'user_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($task) {
            $task->slug = Str::slug($task->title, "-");
        });
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(800);
        $this->addMediaConversion('normal-webp')->width(800)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(1600);
        $this->addMediaConversion('double-webp')->width(1600)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('tasks')->singleFile();
    }

    public function assignment(){
        return $this->belongsTo('App\Models\Assignment');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}