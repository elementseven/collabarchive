<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Assignment extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'status',
        'instructions',
        'project_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($assignment) {
            $assignment->slug = Str::slug($assignment->title, "-");
        });
    }

    public function project(){
        return $this->belongsTo('App\Models\Project');
    }

    public function tasks(){
        return $this->hasMany('App\Models\Task');
    }

}
