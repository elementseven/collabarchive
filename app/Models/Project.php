<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Project extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'slug',
        'status',
        'description',
        'excerpt',
        'meta_description',
        'keywords',
        'photo',
        'aboutimg'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($project) {
            $project->slug = Str::slug($project->title, "-");
        });
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(800);
        $this->addMediaConversion('normal-webp')->width(800)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(1600);
        $this->addMediaConversion('double-webp')->width(1600)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
        $this->addMediaConversion('featured')->keepOriginalImageFormat()->crop('crop-center', 735, 450);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 735, 450)->format('webp');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('projects')->singleFile();
        $this->addMediaCollection('aboutimgs')->singleFile();
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function assignments(){
        return $this->hasMany('App\Models\Assignment');
    }

    public function downloads(){
        return $this->hasMany('App\Models\Download');
    }

}

