<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Download extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'slug',
        'status',
        'type',
        'file',
        'project_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($project) {
            $project->slug = Str::slug($project->title, "-");
        });
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('downloads')->singleFile();
    }

    public function project(){
        return $this->belongsTo('App\Models\Project');
    }
}
