<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\NovaRequest;

class Download extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Download::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';
    public static function label() {
        return 'Project Downloads';
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];
    public static $priority = 3;
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        $types = array('PDF'=>'PDF','Spreadsheet'=>'Spreadsheet','Image'=>'Image','Audio'=>'Audio','Video'=>'Video');
        return [
            BelongsTo::make('Project'),
            Text::make('Title')->sortable(),
            Select::make('Type')->options($types)->sortable()->default('PDF'),
            Select::make('Status')->options($statuses)->sortable()->default('Draft'),
            File::make('File')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('file')->toMediaCollection('downloads');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('downloads', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('downloads', 'thumbnail');
            })->deletable(false)->onlyOnForms(),
            Text::make('Download File', function () {
                return '<a href="'.$this->getFirstMediaUrl('downloads').'" target="_blank"><img src="/img/icons/chevron-double-down.svg" width="8" style="display:inline;margin-right:5px;"> Download</a>';
            })->asHtml()->exceptOnForms(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
