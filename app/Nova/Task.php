<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Trix;
use Froala\NovaFroalaField\Froala;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Task extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Task::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';
    public static $priority = 5;
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array(
            'Ready for volunteer' => 'Ready for volunteer',
            'Submitted'=>'Submitted',
            'Complete'=>'Complete'
        );

        $types = array(
            'Transcription'=>'Transcription',
            'Closed Captions'=>'Closed Captions',
            'Spreadsheet'=>'Spreadsheet',
            'Audio Transcription'=>'Audio Transcription'
        );

        return [
            BelongsTo::make('Assignment')->default(1),
            Select::make('Type')->options($types)->sortable()->default('Transcription'),
            Select::make('Status')->options($statuses)->sortable()->default('Complete'),
            Text::make('Title')->sortable()->default('Page '),
            Number::make('Order')->sortable(),
            Trix::make('Instructions')->nullable(),
            File::make('File')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('file')->toMediaCollection('tasks');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('tasks', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('tasks', 'thumbnail');
            })->deletable(false),
            Text::make('Embed Link','video_link')->hideFromIndex()->help(
                'Embed codes from YouTube, Soundcloud etc. for video & audio transcription'
            ),
            Froala::make('Content')->withFiles('public')->hideWhenCreating(),
            BelongsTo::make('User')->sortable()->default(2),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
