!function(){"use strict";var t="undefined"!=typeof globalThis?globalThis:"undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:{};function e(t,e){return t(e={exports:{}},e.exports),e.exports}var n,r,o=function(t){return t&&t.Math==Math&&t},i=o("object"==typeof globalThis&&globalThis)||o("object"==typeof window&&window)||o("object"==typeof self&&self)||o("object"==typeof t&&t)||function(){return this}()||Function("return this")(),a=function(t){try{return!!t()}catch(t){return!0}},s=!a((function(){return 7!=Object.defineProperty({},1,{get:function(){return 7}})[1]})),c=!a((function(){var t=function(){}.bind();return"function"!=typeof t||t.hasOwnProperty("prototype")})),u=Function.prototype.call,l=c?u.bind(u):function(){return u.apply(u,arguments)},f={}.propertyIsEnumerable,p=Object.getOwnPropertyDescriptor,v={f:p&&!f.call({1:2},1)?function(t){var e=p(this,t);return!!e&&e.enumerable}:f},d=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}},m=Function.prototype,h=m.bind,g=m.call,y=c&&h.bind(g,g),b=c?function(t){return t&&y(t)}:function(t){return t&&function(){return g.apply(t,arguments)}},j=b({}.toString),w=b("".slice),k=function(t){return w(j(t),8,-1)},O=i.Object,S=b("".split),_=a((function(){return!O("z").propertyIsEnumerable(0)}))?function(t){return"String"==k(t)?S(t,""):O(t)}:O,x=i.TypeError,T=function(t){if(null==t)throw x("Can't call method on "+t);return t},P=function(t){return _(T(t))},A=function(t){return"function"==typeof t},L=function(t){return"object"==typeof t?null!==t:A(t)},z=function(t){return A(t)?t:void 0},C=function(t,e){return arguments.length<2?z(i[t]):i[t]&&i[t][e]},E=b({}.isPrototypeOf),I=C("navigator","userAgent")||"",M=i.process,F=i.Deno,D=M&&M.versions||F&&F.version,R=D&&D.v8;R&&(r=(n=R.split("."))[0]>0&&n[0]<4?1:+(n[0]+n[1])),!r&&I&&(!(n=I.match(/Edge\/(\d+)/))||n[1]>=74)&&(n=I.match(/Chrome\/(\d+)/))&&(r=+n[1]);var G=r,N=!!Object.getOwnPropertySymbols&&!a((function(){var t=Symbol();return!String(t)||!(Object(t)instanceof Symbol)||!Symbol.sham&&G&&G<41})),U=N&&!Symbol.sham&&"symbol"==typeof Symbol.iterator,Z=i.Object,B=U?function(t){return"symbol"==typeof t}:function(t){var e=C("Symbol");return A(e)&&E(e.prototype,Z(t))},V=i.String,H=i.TypeError,W=function(t){if(A(t))return t;throw H(function(t){try{return V(t)}catch(t){return"Object"}}(t)+" is not a function")},q=i.TypeError,$=Object.defineProperty,X=function(t,e){try{$(i,t,{value:e,configurable:!0,writable:!0})}catch(n){i[t]=e}return e},Y="__core-js_shared__",Q=i[Y]||X(Y,{}),K=e((function(t){(t.exports=function(t,e){return Q[t]||(Q[t]=void 0!==e?e:{})})("versions",[]).push({version:"3.22.5",mode:"global",copyright:"© 2014-2022 Denis Pushkarev (zloirock.ru)",license:"https://github.com/zloirock/core-js/blob/v3.22.5/LICENSE",source:"https://github.com/zloirock/core-js"})})),J=i.Object,tt=function(t){return J(T(t))},et=b({}.hasOwnProperty),nt=Object.hasOwn||function(t,e){return et(tt(t),e)},rt=0,ot=Math.random(),it=b(1..toString),at=function(t){return"Symbol("+(void 0===t?"":t)+")_"+it(++rt+ot,36)},st=K("wks"),ct=i.Symbol,ut=ct&&ct.for,lt=U?ct:ct&&ct.withoutSetter||at,ft=function(t){if(!nt(st,t)||!N&&"string"!=typeof st[t]){var e="Symbol."+t;N&&nt(ct,t)?st[t]=ct[t]:st[t]=U&&ut?ut(e):lt(e)}return st[t]},pt=i.TypeError,vt=ft("toPrimitive"),dt=function(t,e){if(!L(t)||B(t))return t;var n,r,o=null==(n=t[vt])?void 0:W(n);if(o){if(void 0===e&&(e="default"),r=l(o,t,e),!L(r)||B(r))return r;throw pt("Can't convert object to primitive value")}return void 0===e&&(e="number"),function(t,e){var n,r;if("string"===e&&A(n=t.toString)&&!L(r=l(n,t)))return r;if(A(n=t.valueOf)&&!L(r=l(n,t)))return r;if("string"!==e&&A(n=t.toString)&&!L(r=l(n,t)))return r;throw q("Can't convert object to primitive value")}(t,e)},mt=function(t){var e=dt(t,"string");return B(e)?e:e+""},ht=i.document,gt=L(ht)&&L(ht.createElement),yt=function(t){return gt?ht.createElement(t):{}},bt=!s&&!a((function(){return 7!=Object.defineProperty(yt("div"),"a",{get:function(){return 7}}).a})),jt=Object.getOwnPropertyDescriptor,wt={f:s?jt:function(t,e){if(t=P(t),e=mt(e),bt)try{return jt(t,e)}catch(t){}if(nt(t,e))return d(!l(v.f,t,e),t[e])}},kt=s&&a((function(){return 42!=Object.defineProperty((function(){}),"prototype",{value:42,writable:!1}).prototype})),Ot=i.String,St=i.TypeError,_t=function(t){if(L(t))return t;throw St(Ot(t)+" is not an object")},xt=i.TypeError,Tt=Object.defineProperty,Pt=Object.getOwnPropertyDescriptor,At="enumerable",Lt="configurable",zt="writable",Ct={f:s?kt?function(t,e,n){if(_t(t),e=mt(e),_t(n),"function"==typeof t&&"prototype"===e&&"value"in n&&zt in n&&!n.writable){var r=Pt(t,e);r&&r.writable&&(t[e]=n.value,n={configurable:Lt in n?n.configurable:r.configurable,enumerable:At in n?n.enumerable:r.enumerable,writable:!1})}return Tt(t,e,n)}:Tt:function(t,e,n){if(_t(t),e=mt(e),_t(n),bt)try{return Tt(t,e,n)}catch(t){}if("get"in n||"set"in n)throw xt("Accessors not supported");return"value"in n&&(t[e]=n.value),t}},Et=s?function(t,e,n){return Ct.f(t,e,d(1,n))}:function(t,e,n){return t[e]=n,t},It=Function.prototype,Mt=s&&Object.getOwnPropertyDescriptor,Ft=nt(It,"name"),Dt={EXISTS:Ft,PROPER:Ft&&"something"===function(){}.name,CONFIGURABLE:Ft&&(!s||s&&Mt(It,"name").configurable)},Rt=b(Function.toString);A(Q.inspectSource)||(Q.inspectSource=function(t){return Rt(t)});var Gt,Nt,Ut,Zt=Q.inspectSource,Bt=i.WeakMap,Vt=A(Bt)&&/native code/.test(Zt(Bt)),Ht=K("keys"),Wt=function(t){return Ht[t]||(Ht[t]=at(t))},qt={},$t="Object already initialized",Xt=i.TypeError,Yt=i.WeakMap;if(Vt||Q.state){var Qt=Q.state||(Q.state=new Yt),Kt=b(Qt.get),Jt=b(Qt.has),te=b(Qt.set);Gt=function(t,e){if(Jt(Qt,t))throw new Xt($t);return e.facade=t,te(Qt,t,e),e},Nt=function(t){return Kt(Qt,t)||{}},Ut=function(t){return Jt(Qt,t)}}else{var ee=Wt("state");qt[ee]=!0,Gt=function(t,e){if(nt(t,ee))throw new Xt($t);return e.facade=t,Et(t,ee,e),e},Nt=function(t){return nt(t,ee)?t[ee]:{}},Ut=function(t){return nt(t,ee)}}var ne={set:Gt,get:Nt,has:Ut,enforce:function(t){return Ut(t)?Nt(t):Gt(t,{})},getterFor:function(t){return function(e){var n;if(!L(e)||(n=Nt(e)).type!==t)throw Xt("Incompatible receiver, "+t+" required");return n}}},re=e((function(t){var e=Dt.CONFIGURABLE,n=ne.enforce,r=ne.get,o=Object.defineProperty,i=s&&!a((function(){return 8!==o((function(){}),"length",{value:8}).length})),c=String(String).split("String"),u=t.exports=function(t,r,a){if("Symbol("===String(r).slice(0,7)&&(r="["+String(r).replace(/^Symbol\(([^)]*)\)/,"$1")+"]"),a&&a.getter&&(r="get "+r),a&&a.setter&&(r="set "+r),(!nt(t,"name")||e&&t.name!==r)&&o(t,"name",{value:r,configurable:!0}),i&&a&&nt(a,"arity")&&t.length!==a.arity&&o(t,"length",{value:a.arity}),a&&nt(a,"constructor")&&a.constructor){if(s)try{o(t,"prototype",{writable:!1})}catch(t){}}else t.prototype=void 0;var u=n(t);return nt(u,"source")||(u.source=c.join("string"==typeof r?r:"")),t};Function.prototype.toString=u((function(){return A(this)&&r(this).source||Zt(this)}),"toString")})),oe=function(t,e,n,r){var o=!!r&&!!r.unsafe,a=!!r&&!!r.enumerable,s=!!r&&!!r.noTargetGet,c=r&&void 0!==r.name?r.name:e;return A(n)&&re(n,c,r),t===i?(a?t[e]=n:X(e,n),t):(o?!s&&t[e]&&(a=!0):delete t[e],a?t[e]=n:Et(t,e,n),t)},ie=Math.ceil,ae=Math.floor,se=function(t){var e=+t;return e!=e||0===e?0:(e>0?ae:ie)(e)},ce=Math.max,ue=Math.min,le=Math.min,fe=function(t){return(e=t.length)>0?le(se(e),9007199254740991):0;var e},pe=function(t){return function(e,n,r){var o,i=P(e),a=fe(i),s=function(t,e){var n=se(t);return n<0?ce(n+e,0):ue(n,e)}(r,a);if(t&&n!=n){for(;a>s;)if((o=i[s++])!=o)return!0}else for(;a>s;s++)if((t||s in i)&&i[s]===n)return t||s||0;return!t&&-1}},ve={includes:pe(!0),indexOf:pe(!1)}.indexOf,de=b([].push),me=function(t,e){var n,r=P(t),o=0,i=[];for(n in r)!nt(qt,n)&&nt(r,n)&&de(i,n);for(;e.length>o;)nt(r,n=e[o++])&&(~ve(i,n)||de(i,n));return i},he=["constructor","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","toLocaleString","toString","valueOf"],ge=he.concat("length","prototype"),ye={f:Object.getOwnPropertyNames||function(t){return me(t,ge)}},be={f:Object.getOwnPropertySymbols},je=b([].concat),we=C("Reflect","ownKeys")||function(t){var e=ye.f(_t(t)),n=be.f;return n?je(e,n(t)):e},ke=function(t,e,n){for(var r=we(e),o=Ct.f,i=wt.f,a=0;a<r.length;a++){var s=r[a];nt(t,s)||n&&nt(n,s)||o(t,s,i(e,s))}},Oe=/#|\.prototype\./,Se=function(t,e){var n=xe[_e(t)];return n==Pe||n!=Te&&(A(e)?a(e):!!e)},_e=Se.normalize=function(t){return String(t).replace(Oe,".").toLowerCase()},xe=Se.data={},Te=Se.NATIVE="N",Pe=Se.POLYFILL="P",Ae=Se,Le=wt.f,ze=function(t,e){var n,r,o,a,s,c=t.target,u=t.global,l=t.stat;if(n=u?i:l?i[c]||X(c,{}):(i[c]||{}).prototype)for(r in e){if(a=e[r],o=t.noTargetGet?(s=Le(n,r))&&s.value:n[r],!Ae(u?r:c+(l?".":"#")+r,t.forced)&&void 0!==o){if(typeof a==typeof o)continue;ke(a,o)}(t.sham||o&&o.sham)&&Et(a,"sham",!0),oe(n,r,a,t)}},Ce=Array.isArray||function(t){return"Array"==k(t)},Ee=function(t,e,n){var r=mt(e);r in t?Ct.f(t,r,d(0,n)):t[r]=n},Ie={};Ie[ft("toStringTag")]="z";var Me="[object z]"===String(Ie),Fe=ft("toStringTag"),De=i.Object,Re="Arguments"==k(function(){return arguments}()),Ge=Me?k:function(t){var e,n,r;return void 0===t?"Undefined":null===t?"Null":"string"==typeof(n=function(t,e){try{return t[e]}catch(t){}}(e=De(t),Fe))?n:Re?k(e):"Object"==(r=k(e))&&A(e.callee)?"Arguments":r},Ne=function(){},Ue=[],Ze=C("Reflect","construct"),Be=/^\s*(?:class|function)\b/,Ve=b(Be.exec),He=!Be.exec(Ne),We=function(t){if(!A(t))return!1;try{return Ze(Ne,Ue,t),!0}catch(t){return!1}},qe=function(t){if(!A(t))return!1;switch(Ge(t)){case"AsyncFunction":case"GeneratorFunction":case"AsyncGeneratorFunction":return!1}try{return He||!!Ve(Be,Zt(t))}catch(t){return!0}};qe.sham=!0;var $e,Xe=!Ze||a((function(){var t;return We(We.call)||!We(Object)||!We((function(){t=!0}))||t}))?qe:We,Ye=ft("species"),Qe=i.Array,Ke=function(t,e){return new(function(t){var e;return Ce(t)&&(e=t.constructor,(Xe(e)&&(e===Qe||Ce(e.prototype))||L(e)&&null===(e=e[Ye]))&&(e=void 0)),void 0===e?Qe:e}(t))(0===e?0:e)},Je=ft("species"),tn=ft("isConcatSpreadable"),en=9007199254740991,nn="Maximum allowed index exceeded",rn=i.TypeError,on=G>=51||!a((function(){var t=[];return t[tn]=!1,t.concat()[0]!==t})),an=($e="concat",G>=51||!a((function(){var t=[];return(t.constructor={})[Je]=function(){return{foo:1}},1!==t[$e](Boolean).foo}))),sn=function(t){if(!L(t))return!1;var e=t[tn];return void 0!==e?!!e:Ce(t)};ze({target:"Array",proto:!0,arity:1,forced:!on||!an},{concat:function(t){var e,n,r,o,i,a=tt(this),s=Ke(a,0),c=0;for(e=-1,r=arguments.length;e<r;e++)if(sn(i=-1===e?a:arguments[e])){if(c+(o=fe(i))>en)throw rn(nn);for(n=0;n<o;n++,c++)n in i&&Ee(s,c,i[n])}else{if(c>=en)throw rn(nn);Ee(s,c++,i)}return s.length=c,s}});var cn,un=Object.keys||function(t){return me(t,he)},ln=s&&!kt?Object.defineProperties:function(t,e){_t(t);for(var n,r=P(e),o=un(e),i=o.length,a=0;i>a;)Ct.f(t,n=o[a++],r[n]);return t},fn={f:ln},pn=C("document","documentElement"),vn=Wt("IE_PROTO"),dn=function(){},mn=function(t){return"<script>"+t+"</"+"script>"},hn=function(t){t.write(mn("")),t.close();var e=t.parentWindow.Object;return t=null,e},gn=function(){try{cn=new ActiveXObject("htmlfile")}catch(t){}var t,e;gn="undefined"!=typeof document?document.domain&&cn?hn(cn):((e=yt("iframe")).style.display="none",pn.appendChild(e),e.src=String("javascript:"),(t=e.contentWindow.document).open(),t.write(mn("document.F=Object")),t.close(),t.F):hn(cn);for(var n=he.length;n--;)delete gn.prototype[he[n]];return gn()};qt[vn]=!0;var yn=Object.create||function(t,e){var n;return null!==t?(dn.prototype=_t(t),n=new dn,dn.prototype=null,n[vn]=t):n=gn(),void 0===e?n:fn.f(n,e)},bn=ft("unscopables"),jn=Array.prototype;null==jn[bn]&&Ct.f(jn,bn,{configurable:!0,value:yn(null)});var wn,kn,On,Sn=function(t){jn[bn][t]=!0},_n={},xn=!a((function(){function t(){}return t.prototype.constructor=null,Object.getPrototypeOf(new t)!==t.prototype})),Tn=Wt("IE_PROTO"),Pn=i.Object,An=Pn.prototype,Ln=xn?Pn.getPrototypeOf:function(t){var e=tt(t);if(nt(e,Tn))return e[Tn];var n=e.constructor;return A(n)&&e instanceof n?n.prototype:e instanceof Pn?An:null},zn=ft("iterator"),Cn=!1;[].keys&&("next"in(On=[].keys())?(kn=Ln(Ln(On)))!==Object.prototype&&(wn=kn):Cn=!0);var En=null==wn||a((function(){var t={};return wn[zn].call(t)!==t}));En&&(wn={}),A(wn[zn])||oe(wn,zn,(function(){return this}));var In={IteratorPrototype:wn,BUGGY_SAFARI_ITERATORS:Cn},Mn=Ct.f,Fn=ft("toStringTag"),Dn=function(t,e,n){t&&!n&&(t=t.prototype),t&&!nt(t,Fn)&&Mn(t,Fn,{configurable:!0,value:e})},Rn=In.IteratorPrototype,Gn=function(){return this},Nn=i.String,Un=i.TypeError,Zn=Object.setPrototypeOf||("__proto__"in{}?function(){var t,e=!1,n={};try{(t=b(Object.getOwnPropertyDescriptor(Object.prototype,"__proto__").set))(n,[]),e=n instanceof Array}catch(t){}return function(n,r){return _t(n),function(t){if("object"==typeof t||A(t))return t;throw Un("Can't set "+Nn(t)+" as a prototype")}(r),e?t(n,r):n.__proto__=r,n}}():void 0),Bn=Dt.PROPER,Vn=Dt.CONFIGURABLE,Hn=In.IteratorPrototype,Wn=In.BUGGY_SAFARI_ITERATORS,qn=ft("iterator"),$n="keys",Xn="values",Yn="entries",Qn=function(){return this},Kn=Ct.f,Jn="Array Iterator",tr=ne.set,er=ne.getterFor(Jn),nr=function(t,e,n,r,o,i,a){!function(t,e,n,r){var o=e+" Iterator";t.prototype=yn(Rn,{next:d(+!r,n)}),Dn(t,o,!1),_n[o]=Gn}(n,e,r);var s,c,u,f=function(t){if(t===o&&g)return g;if(!Wn&&t in m)return m[t];switch(t){case $n:case Xn:case Yn:return function(){return new n(this,t)}}return function(){return new n(this)}},p=e+" Iterator",v=!1,m=t.prototype,h=m[qn]||m["@@iterator"]||o&&m[o],g=!Wn&&h||f(o),y="Array"==e&&m.entries||h;if(y&&(s=Ln(y.call(new t)))!==Object.prototype&&s.next&&(Ln(s)!==Hn&&(Zn?Zn(s,Hn):A(s[qn])||oe(s,qn,Qn)),Dn(s,p,!0)),Bn&&o==Xn&&h&&h.name!==Xn&&(Vn?Et(m,"name",Xn):(v=!0,g=function(){return l(h,this)})),o)if(c={values:f(Xn),keys:i?g:f($n),entries:f(Yn)},a)for(u in c)(Wn||v||!(u in m))&&oe(m,u,c[u]);else ze({target:e,proto:!0,forced:Wn||v},c);return m[qn]!==g&&oe(m,qn,g,{name:o}),_n[e]=g,c}(Array,"Array",(function(t,e){tr(this,{type:Jn,target:P(t),index:0,kind:e})}),(function(){var t=er(this),e=t.target,n=t.kind,r=t.index++;return!e||r>=e.length?(t.target=void 0,{value:void 0,done:!0}):"keys"==n?{value:r,done:!1}:"values"==n?{value:e[r],done:!1}:{value:[r,e[r]],done:!1}}),"values"),rr=_n.Arguments=_n.Array;if(Sn("keys"),Sn("values"),Sn("entries"),s&&"values"!==rr.name)try{Kn(rr,"name",{value:"values"})}catch(t){}var or=Me?{}.toString:function(){return"[object "+Ge(this)+"]"};Me||oe(Object.prototype,"toString",or,{unsafe:!0});var ir={CSSRuleList:0,CSSStyleDeclaration:0,CSSValueList:0,ClientRectList:0,DOMRectList:0,DOMStringList:0,DOMTokenList:1,DataTransferItemList:0,FileList:0,HTMLAllCollection:0,HTMLCollection:0,HTMLFormElement:0,HTMLSelectElement:0,MediaList:0,MimeTypeArray:0,NamedNodeMap:0,NodeList:1,PaintRequestList:0,Plugin:0,PluginArray:0,SVGLengthList:0,SVGNumberList:0,SVGPathSegList:0,SVGPointList:0,SVGStringList:0,SVGTransformList:0,SourceBufferList:0,StyleSheetList:0,TextTrackCueList:0,TextTrackList:0,TouchList:0},ar=yt("span").classList,sr=ar&&ar.constructor&&ar.constructor.prototype,cr=sr===Object.prototype?void 0:sr,ur=ft("iterator"),lr=ft("toStringTag"),fr=nr.values,pr=function(t,e){if(t){if(t[ur]!==fr)try{Et(t,ur,fr)}catch(e){t[ur]=fr}if(t[lr]||Et(t,lr,e),ir[e])for(var n in nr)if(t[n]!==nr[n])try{Et(t,n,nr[n])}catch(e){t[n]=nr[n]}}};for(var vr in ir)pr(i[vr]&&i[vr].prototype,vr);pr(cr,"DOMTokenList");var dr=a((function(){un(1)}));ze({target:"Object",stat:!0,forced:dr},{keys:function(t){return un(tt(t))}}),(self.webpackChunk=self.webpackChunk||[]).push([[496],{49147:function(t,e,n){window._=n(96486);try{window.Popper=n(28981).default,window.$=window.jQuery=n(19755),n(43734)}catch(t){}window.axios=n(9669),window.axios.defaults.headers.common["X-Requested-With"]="XMLHttpRequest"},70589:function(t,e,n){var r=n(55464),o=n.n(r),i=(n(76124),n(67356)),a=n(78345),s={components:{},props:{user:Object,source:String},data:function(){return{userob:this.user,overlay:!1,zIndex:1e3,links:["Home","About Us","Team","Services","Blog","Contact Us"],alert:{type:"",message:""},dialog:!1,loader:!1,drawer:null,alertStatus:!1,pageType:"dark",page:"home",title:""}},mounted:function(){},updated:function(){},methods:{showAlert:function(t){this.alertStatus=!0,this.alert.type=t.type,this.alert.message=t.message},pageChanged:function(t,e,n){this.pageType=t.type,this.page=e.page,this.title=e.title},showLoader:function(){this.loader=!0},hideLoader:function(){this.loader=!1},backToTop:function(){setTimeout((function(){document.body.scrollTop=document.documentElement.scrollTop=0}),450)}}},c=n(93379),u=n.n(c),l=n(62583),f={insert:"head",singleton:!1},p=(u()(l.Z,f),l.Z.locals,n(68422)),v={insert:"head",singleton:!1},d=(u()(p.Z,v),p.Z.locals,(0,n(51900).Z)(s,(function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("v-app",{attrs:{id:"app"}},[n("v-navigation-drawer",{attrs:{clipped:t.$vuetify.breakpoint.lgAndUp,app:""},model:{value:t.drawer,callback:function(e){t.drawer=e},expression:"drawer"}},[n("v-list",{attrs:{dense:""}},[n("v-row",{attrs:{align:"center"}},[n("v-col",{attrs:{cols:"12"}},[n("router-link",{attrs:{to:{name:"Dashboard"}},nativeOn:{click:function(e){return t.backToTop()}}},[n("v-list-item",{attrs:{link:"",light:""}},[n("v-list-item-action",[n("v-icon",[t._v("mdi-view-dashboard")])],1),t._v(" "),n("v-list-item-content",[n("v-list-item-title",[t._v("Dashboard")])],1)],1)],1),t._v(" "),n("router-link",{attrs:{to:{name:"Projects"}},nativeOn:{click:function(e){return t.backToTop()}}},[n("v-list-item",{attrs:{link:"",light:""}},[n("v-list-item-action",[n("v-icon",[t._v("mdi-clipboard-file-outline")])],1),t._v(" "),n("v-list-item-content",[n("v-list-item-title",[t._v("Projects")])],1)],1)],1),t._v(" "),n("router-link",{attrs:{to:{name:"Account"}},nativeOn:{click:function(e){return t.backToTop()}}},[n("v-list-item",{attrs:{link:"",light:""}},[n("v-list-item-action",[n("v-icon",[t._v("mdi-account")])],1),t._v(" "),n("v-list-item-content",[n("v-list-item-title",[t._v("Account Management")])],1)],1)],1),t._v(" "),n("a",{attrs:{href:"/logout"}},[n("v-list-item",{attrs:{link:"",light:""}},[n("v-list-item-action",[n("v-icon",[t._v("mdi-logout")])],1),t._v(" "),n("v-list-item-content",[n("v-list-item-title",[t._v("Logout")])],1)],1)],1)],1)],1)],1)],1),t._v(" "),n("v-app-bar",{attrs:{"clipped-left":t.$vuetify.breakpoint.lgAndUp,app:"",color:"white",light:""}},[n("v-app-bar-nav-icon",{on:{click:function(e){e.stopPropagation(),t.drawer=!t.drawer}}}),t._v(" "),n("v-toolbar-title",{staticClass:"ml-0 pl-4 pt-2",staticStyle:{width:"300px"}},[n("a",{attrs:{href:"/",target:"_blank"}},[n("img",{attrs:{src:"/img/logos/logo.svg?v1.0",alt:"QuoPro logo",width:"100"}})])]),t._v(" "),n("v-spacer"),t._v(" "),n("a",{attrs:{href:"/logout",target:"_blank"}},[n("v-btn",{staticClass:"fw-600 d-none d-md-block",attrs:{text:"",color:"primary"}},[t._v("Logout")])],1)],1),t._v(" "),n("v-main",[n("v-container",{staticClass:"py-5  px-5 footer-margin"},[n("v-row",{staticClass:"py-5"},[t.alertStatus?n("v-col",{staticClass:"mb-0",attrs:{cols:"12"}},[n("v-alert",{attrs:{dense:"",text:"",dismissible:"",type:t.alert.type},model:{value:t.alertStatus,callback:function(e){t.alertStatus=e},expression:"alertStatus"}},[t._v(t._s(t.alert.message))])],1):t._e(),t._v(" "),"Dashboard"==t.title?n("v-col",{staticClass:"py-5 my-5 mob-py-0 mob-my-0 dash-title",attrs:{cols:"12"}},[n("h2",{staticClass:"text-dark"},[t._v("Hi "+t._s(t.user.first_name))]),t._v(" "),n("v-divider",{staticClass:"my-3"}),t._v(" "),n("p",{staticClass:"text-dark"},[t._v("Welcome to your CollabArchive dashboard, from here you can "),n("u",[n("router-link",{attrs:{to:{name:"Projects"}},nativeOn:{click:function(e){return t.backToTop()}}},[t._v("complete project tasks")])],1),t._v(" and "),n("u",[n("router-link",{attrs:{to:{name:"Account"}},nativeOn:{click:function(e){return t.backToTop()}}},[t._v("manage your account")])],1),t._v(".")])],1):t._e(),t._v(" "),n("transition",{attrs:{name:"fade",mode:"out-in",appear:""}},[n("router-view",{key:t.$route.path,on:{showLoader:t.showLoader,hideLoader:t.hideLoader,pageChange:function(e,n){for(var r=arguments.length,o=Array(r);r--;)o[r]=arguments[r];t.pageChanged.apply(void 0,["extra"].concat(o))},showAlert:t.showAlert}})],1)],1)],1),t._v(" "),"Task"!=t.title?n("v-footer",{attrs:{color:"secondary",padless:"",absolute:"",bottom:""}},[n("v-row",{attrs:{justify:"center","no-gutters":""}},[n("v-col",{staticClass:"secondary lighten-2 py-4 text-center white--text",attrs:{cols:"12"}},[t._v("\n\t\t        "+t._s((new Date).getFullYear())+" — "),n("strong",[t._v("CollabArchive")]),t._v(" - Website by "),n("a",{staticStyle:{color:"white"},attrs:{href:"https://elementseven.co"}},[n("u",[t._v("Element Seven")])])])],1)],1):t._e()],1),t._v(" "),n("v-overlay",{attrs:{value:t.loader}},[n("v-progress-circular",{attrs:{indeterminate:"",size:"64"}})],1)],1)}),[],!1,null,null,null)),m=d.exports,h=n(54826),g=n.n(h),y=n(70538);n(49147),n(19755),window.moment=n(30381),y.default.use(o()),y.default.use(a.Z),y.default.use(i.Z,{refreshOnceOnNavigation:!0}),y.default.use(g()),y.default.component("laravel-pagination",n(31606));var b=new a.Z({mode:"history",routes:[{path:"/users/dashboard",name:"Dashboard",component:function(){return n.e(438).then(n.bind(n,99280))}},{path:"/users/account",name:"Account",component:function(){return n.e(622).then(n.bind(n,76090))}},{path:"/users/projects",name:"Projects",component:function(){return n.e(780).then(n.bind(n,60451))}},{path:"/users/projects/:projectSlug",name:"Project",component:function(){return n.e(735).then(n.bind(n,56323))}},{path:"/users/tasks/:taskId/:taskSlug",name:"Task",component:function(){return n.e(939).then(n.bind(n,44850))}}]});new y.default({el:"#app",components:{App:m},router:b,vuetify:new(o())({theme:{themes:{light:{primary:"#D95C92",secondary:"#5C478F",green:"#50EA9D",accent:"#D95C92",error:"#b71c1c"}}}})})},62583:function(t,e,n){n.d(e,{Z:function(){return c}});var r=n(23645),o=n.n(r),i=n(11629),a=n(88939),s=o()((function(t){return t[1]}));s.i(i.Z),s.i(a.Z),s.push([t.id,"a{text-decoration:none!important}.v-application{font-family:nunito-sans,sans-serif}.v-main{background-color:#fff;color:#5c478f}h1{font-size:2.5rem}h2{font-size:2rem}h2.page-title{color:#8dc3ff;font-weight:300!important;letter-spacing:6px;padding-top:1rem;text-transform:uppercase}h2.school-name{font-size:1.8rem}h1,h2,h3,h4{font-family:objektiv-mk1,sans-serif;font-weight:600!important}h2.user-name{font-size:3rem;line-height:1}p{color:#0f0a4d}p b{font-weight:600}p.text-large{font-size:18px}p.text-small{font-size:14px}.v-btn{letter-spacing:0}.fw-600,.v-btn{font-weight:600!important}.text-pink{color:#d95c92!important}.text-dark{color:#0f0a4d!important}.theme--light.v-divider{border-color:#5c478f!important}.v-sheet.v-app-bar.v-toolbar:not(.v-sheet--outlined),.v-sheet.v-card:not(.v-sheet--outlined){box-shadow:0 0 .5rem rgba(0,0,0,.1)!important}.v-card__title{font-size:1.1rem;font-weight:600!important}.theme--light.v-card .v-card__subtitle,.theme--light.v-card>.v-card__text,.v-card__title{color:#0f0a4d!important}.border-left{border-left:1px solid #d95c92}.theme--light.v-navigation-drawer:not(.v-navigation-drawer--floating) .v-navigation-drawer__border{background-color:transparent!important}.round-image{border-radius:100%;display:inline-block;height:100px;margin-bottom:-17px;overflow:hidden;width:100px}.footer-margin{margin-bottom:100px}.theme--light.v-navigation-drawer{background-color:#352774!important}.theme--light nav .v-list-item:not(.v-list-item--active):not(.v-list-item--disabled),.theme--light.v-navigation-drawer .theme--light.v-icon,.v-application .v-list-group--active.primary--text{color:#fff!important}@media only screen and (max-width:767px){.v-application .mob-py-0{padding-bottom:0!important;padding-top:0!important}.v-application .mob-my-0{margin-bottom:0!important;margin-top:0!important}.footer-margin{margin-bottom:200px}#customer-avatar,.round-image{height:50px!important;margin-bottom:0;width:50px!important}h2.user-name{font-size:7vw}}",""]);var c=s},68422:function(t,e,n){n.d(e,{Z:function(){return i}});var r=n(23645),o=n.n(r)()((function(t){return t[1]}));o.push([t.id,".dash-title .v-avatar{box-shadow:0 0 .5rem rgba(29,61,141,.2)!important;margin-top:-40px}",""]);var i=o},65664:function(){},46700:function(t,e,n){var r={"./af":42786,"./af.js":42786,"./ar":30867,"./ar-dz":14130,"./ar-dz.js":14130,"./ar-kw":96135,"./ar-kw.js":96135,"./ar-ly":56440,"./ar-ly.js":56440,"./ar-ma":47702,"./ar-ma.js":47702,"./ar-sa":82705,"./ar-sa.js":82705,"./ar-tn":37100,"./ar-tn.js":37100,"./ar.js":30867,"./az":31083,"./az.js":31083,"./be":9808,"./be.js":9808,"./bg":68338,"./bg.js":68338,"./bm":67438,"./bm.js":67438,"./bn":8905,"./bn-bd":76225,"./bn-bd.js":76225,"./bn.js":8905,"./bo":11560,"./bo.js":11560,"./br":1278,"./br.js":1278,"./bs":80622,"./bs.js":80622,"./ca":2468,"./ca.js":2468,"./cs":5822,"./cs.js":5822,"./cv":50877,"./cv.js":50877,"./cy":47373,"./cy.js":47373,"./da":24780,"./da.js":24780,"./de":59740,"./de-at":60217,"./de-at.js":60217,"./de-ch":60894,"./de-ch.js":60894,"./de.js":59740,"./dv":5300,"./dv.js":5300,"./el":50837,"./el.js":50837,"./en-au":51894,"./en-au.js":51894,"./en-ca":77925,"./en-ca.js":77925,"./en-gb":22243,"./en-gb.js":22243,"./en-ie":46436,"./en-ie.js":46436,"./en-il":47207,"./en-il.js":47207,"./en-in":44175,"./en-in.js":44175,"./en-nz":76319,"./en-nz.js":76319,"./en-sg":31662,"./en-sg.js":31662,"./eo":92915,"./eo.js":92915,"./es":55655,"./es-do":55251,"./es-do.js":55251,"./es-mx":96112,"./es-mx.js":96112,"./es-us":71146,"./es-us.js":71146,"./es.js":55655,"./et":5603,"./et.js":5603,"./eu":77763,"./eu.js":77763,"./fa":76959,"./fa.js":76959,"./fi":11897,"./fi.js":11897,"./fil":42549,"./fil.js":42549,"./fo":94694,"./fo.js":94694,"./fr":94470,"./fr-ca":63049,"./fr-ca.js":63049,"./fr-ch":52330,"./fr-ch.js":52330,"./fr.js":94470,"./fy":5044,"./fy.js":5044,"./ga":29295,"./ga.js":29295,"./gd":2101,"./gd.js":2101,"./gl":38794,"./gl.js":38794,"./gom-deva":27884,"./gom-deva.js":27884,"./gom-latn":23168,"./gom-latn.js":23168,"./gu":95349,"./gu.js":95349,"./he":24206,"./he.js":24206,"./hi":30094,"./hi.js":30094,"./hr":30316,"./hr.js":30316,"./hu":22138,"./hu.js":22138,"./hy-am":11423,"./hy-am.js":11423,"./id":29218,"./id.js":29218,"./is":90135,"./is.js":90135,"./it":90626,"./it-ch":10150,"./it-ch.js":10150,"./it.js":90626,"./ja":39183,"./ja.js":39183,"./jv":24286,"./jv.js":24286,"./ka":12105,"./ka.js":12105,"./kk":47772,"./kk.js":47772,"./km":18758,"./km.js":18758,"./kn":79282,"./kn.js":79282,"./ko":33730,"./ko.js":33730,"./ku":1408,"./ku.js":1408,"./ky":33291,"./ky.js":33291,"./lb":36841,"./lb.js":36841,"./lo":55466,"./lo.js":55466,"./lt":57010,"./lt.js":57010,"./lv":37595,"./lv.js":37595,"./me":39861,"./me.js":39861,"./mi":35493,"./mi.js":35493,"./mk":95966,"./mk.js":95966,"./ml":87341,"./ml.js":87341,"./mn":5115,"./mn.js":5115,"./mr":10370,"./mr.js":10370,"./ms":9847,"./ms-my":41237,"./ms-my.js":41237,"./ms.js":9847,"./mt":72126,"./mt.js":72126,"./my":56165,"./my.js":56165,"./nb":64924,"./nb.js":64924,"./ne":16744,"./ne.js":16744,"./nl":93901,"./nl-be":59814,"./nl-be.js":59814,"./nl.js":93901,"./nn":83877,"./nn.js":83877,"./oc-lnc":92135,"./oc-lnc.js":92135,"./pa-in":15858,"./pa-in.js":15858,"./pl":64495,"./pl.js":64495,"./pt":89520,"./pt-br":57971,"./pt-br.js":57971,"./pt.js":89520,"./ro":96459,"./ro.js":96459,"./ru":21793,"./ru.js":21793,"./sd":40950,"./sd.js":40950,"./se":10490,"./se.js":10490,"./si":90124,"./si.js":90124,"./sk":64249,"./sk.js":64249,"./sl":14985,"./sl.js":14985,"./sq":51104,"./sq.js":51104,"./sr":49131,"./sr-cyrl":79915,"./sr-cyrl.js":79915,"./sr.js":49131,"./ss":85893,"./ss.js":85893,"./sv":98760,"./sv.js":98760,"./sw":91172,"./sw.js":91172,"./ta":27333,"./ta.js":27333,"./te":23110,"./te.js":23110,"./tet":52095,"./tet.js":52095,"./tg":27321,"./tg.js":27321,"./th":9041,"./th.js":9041,"./tk":19005,"./tk.js":19005,"./tl-ph":75768,"./tl-ph.js":75768,"./tlh":89444,"./tlh.js":89444,"./tr":72397,"./tr.js":72397,"./tzl":28254,"./tzl.js":28254,"./tzm":51106,"./tzm-latn":30699,"./tzm-latn.js":30699,"./tzm.js":51106,"./ug-cn":9288,"./ug-cn.js":9288,"./uk":67691,"./uk.js":67691,"./ur":13795,"./ur.js":13795,"./uz":6791,"./uz-latn":60588,"./uz-latn.js":60588,"./uz.js":6791,"./vi":65666,"./vi.js":65666,"./x-pseudo":14378,"./x-pseudo.js":14378,"./yo":75805,"./yo.js":75805,"./zh-cn":83839,"./zh-cn.js":83839,"./zh-hk":55726,"./zh-hk.js":55726,"./zh-mo":99807,"./zh-mo.js":99807,"./zh-tw":74152,"./zh-tw.js":74152};function o(t){var e=i(t);return n(e)}function i(t){if(!n.o(r,t)){var e=new Error("Cannot find module '"+t+"'");throw e.code="MODULE_NOT_FOUND",e}return r[t]}o.keys=function(){return Object.keys(r)},o.resolve=i,t.exports=o,o.id=46700}},function(t){var e=function(e){return t(t.s=e)};t.O(0,[828,431],(function(){return e(70589),e(65664)}));t.O()}])}();