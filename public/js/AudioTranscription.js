(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["AudioTranscription"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue2_editor__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue2-editor */"./node_modules/vue2-editor/dist/vue2-editor.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
task:Object},

components:{
VueEditor:vue2_editor__WEBPACK_IMPORTED_MODULE_0__.VueEditor},

data:function data(){
return {
dialog:false,
content:this.task.content,
customToolbar:[["bold","italic","underline"],[{
list:"bullet"}],
["clean"]]};

},
mounted:function mounted(){
this.$emit('pageChange',{
'type':'dark',
'page':'Account',
'title':'Account'});

},
methods:{
save:function save(){
var _this=this;

this.$emit('showLoader');
this.loaded=false;
this.errors={};
var formData=new FormData();
formData.append('content',this.content);
axios.post('/tasks/save/'+this.task.id,formData,{
headers:{
'Content-Type':'multipart/form-data'}}).

then(function(response){
// Empty fields and enable success message
_this.loaded=true;

_this.$emit('hideLoader');

_this.$emit('showAlert',{
'type':'success',
'message':'Progress Saved'});


_this.backToTop();
})["catch"](function(error){
// Error handling
_this.loaded=true;
console.log(error);

_this.$emit('hideLoader');

_this.$emit('showAlert',{
'type':'error',
'message':'There was an error saving your progress'});


if(error.response.status===422){
_this.errors=error.response.data.errors||{};
}
});
},
submit:function submit(){
var _this2=this;

this.$emit('showLoader');
this.loaded=false;
this.errors={};
var formData=new FormData();
formData.append('content',this.content);
axios.post('/tasks/submit/'+this.task.id,formData,{
headers:{
'Content-Type':'multipart/form-data'}}).

then(function(response){
// Empty fields and enable success message
_this2.loaded=true;

_this2.$emit('hideLoader');

_this2.$emit('showAlert',{
'type':'success',
'message':'Task Submitted'});


_this2.dialog=false;

_this2.$router.push({
name:'Project',
params:{
projectSlug:_this2.task.assignment.project.slug}});


})["catch"](function(error){
// Error handling
_this2.loaded=true;
console.log(error);
_this2.dialog=false;

_this2.$emit('hideLoader');

_this2.$emit('showAlert',{
'type':'error',
'message':'There was an error submitting your task'});

});
},
backToTop:function backToTop(){
setTimeout(function(){
document.body.scrollTop=document.documentElement.scrollTop=0;
},450);
},
showAlert:function showAlert(value){
this.$emit('showAlert',{
'type':value.type,
'message':value.message});

},
showLoader:function showLoader(){
this.$emit('showLoader');
},
hideLoader:function hideLoader(){
this.$emit('hideLoader');
}}};



/***/},

/***/"./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */"./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".ql-editor {\n  min-height: 685px !important;\n}\n.ql-editor p {\n    color: #000000 !important;\n}\n.embed-responsive {\n  position: relative;\n  display: block;\n  width: 100%;\n  padding: 0;\n  overflow: hidden;\n}\n.embed-responsive iframe {\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    border: 0;\n}\n.embed-responsive.embed-responsive-16by9::before {\n    display: block;\n    content: \"\";\n    padding-top: 56.25%;\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderDistCjsJsNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AudioTranscription.vue?vue&type=style&index=0&lang=scss& */"./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/users/components/tasks/AudioTranscription.vue":
/*!********************************************************************!*\
  !*** ./resources/js/users/components/tasks/AudioTranscription.vue ***!
  \********************************************************************/
/***/function resourcesJsUsersComponentsTasksAudioTranscriptionVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _AudioTranscription_vue_vue_type_template_id_70e990e1___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./AudioTranscription.vue?vue&type=template&id=70e990e1& */"./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=template&id=70e990e1&");
/* harmony import */var _AudioTranscription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./AudioTranscription.vue?vue&type=script&lang=js& */"./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=script&lang=js&");
/* harmony import */var _AudioTranscription_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./AudioTranscription.vue?vue&type=style&index=0&lang=scss& */"./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_AudioTranscription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_AudioTranscription_vue_vue_type_template_id_70e990e1___WEBPACK_IMPORTED_MODULE_0__.render,
_AudioTranscription_vue_vue_type_template_id_70e990e1___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/users/components/tasks/AudioTranscription.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/***/function resourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AudioTranscription.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************/
/***/function resourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AudioTranscription.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=template&id=70e990e1&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=template&id=70e990e1& ***!
  \***************************************************************************************************/
/***/function resourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeTemplateId70e990e1(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_template_id_70e990e1___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_template_id_70e990e1___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AudioTranscription_vue_vue_type_template_id_70e990e1___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AudioTranscription.vue?vue&type=template&id=70e990e1& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=template&id=70e990e1&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=template&id=70e990e1&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/users/components/tasks/AudioTranscription.vue?vue&type=template&id=70e990e1& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsUsersComponentsTasksAudioTranscriptionVueVueTypeTemplateId70e990e1(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"v-container",
[
_c(
"v-row",
[
_c("v-col",{attrs:{cols:"8"}},[
_c("h3",{staticClass:"text-dark mb-2"},[
_vm._v(_vm._s(_vm.task.type)+" - "+_vm._s(_vm.task.title))]),

_vm._v(" "),
_vm.task.assignment.instructions?
_c("p",{staticClass:"text-dark mb-2"},[
_vm._v(_vm._s(_vm.task.assignment.instructions))]):

_vm._e(),
_vm._v(" "),
_vm.task.instructions?
_c("p",{
staticClass:"text-dark mb-0",
domProps:{innerHTML:_vm._s(_vm.task.instructions)}}):

_vm._e()]),

_vm._v(" "),
_c(
"v-col",
{staticClass:"text-right",attrs:{cols:"4"}},
[
_c(
"v-btn",
{
staticClass:"mr-2",
attrs:{color:"primary"},
on:{click:_vm.save}},

[_vm._v("Save")]),

_vm._v(" "),
_c(
"v-btn",
{
attrs:{color:"green"},
on:{
click:function click($event){
$event.stopPropagation();
_vm.dialog=true;
}}},


[_vm._v("Submit Task")])],


1),

_vm._v(" "),
_c(
"v-col",
{attrs:{cols:"12 mb-5"}},
[_c("v-divider",{staticClass:"my-0"})],
1),

_vm._v(" "),
_c("v-col",{attrs:{cols:"6"}},[
_c("audio",{
ref:"audio",
attrs:{
src:_vm.task.file.original_url,
preload:"auto",
volume:"1",
muted:"",
loop:""}}),


_vm._v(" "),
_c("div",{
staticClass:"embed-responsive embed-responsive-16by9",
domProps:{innerHTML:_vm._s(_vm.task.video_link)}})]),


_vm._v(" "),
_c(
"v-col",
{attrs:{cols:"6"}},
[
_c("vue-editor",{
attrs:{editorToolbar:_vm.customToolbar},
model:{
value:_vm.content,
callback:function callback($$v){
_vm.content=$$v;
},
expression:"content"}})],



1)],


1),

_vm._v(" "),
_c(
"v-footer",
{attrs:{color:"secondary",inset:"",bottom:"",absolute:""}},
[
_c(
"v-container",
{staticClass:"py-0"},
[
_c(
"v-row",
{attrs:{justify:"center","no-gutters":""}},
[
_c(
"v-col",
{staticClass:"py-4 white--text",attrs:{cols:"6"}},
[
_c(
"p",
{
staticClass:"text-large white--text mb-0",
attrs:{color:"white"}},

[_vm._v("Don't forget to save your work.")])]),



_vm._v(" "),
_c(
"v-col",
{staticClass:"text-right pt-3",attrs:{cols:"6"}},
[
_c(
"v-btn",
{
staticClass:"mr-2",
attrs:{color:"primary"},
on:{click:_vm.save}},

[_vm._v("Save")]),

_vm._v(" "),
_c(
"v-btn",
{
attrs:{color:"green"},
on:{
click:function click($event){
$event.stopPropagation();
_vm.dialog=true;
}}},


[_vm._v("Submit Task")])],


1)],


1)],


1)],


1),

_vm._v(" "),
_c(
"v-dialog",
{
attrs:{"max-width":"290"},
model:{
value:_vm.dialog,
callback:function callback($$v){
_vm.dialog=$$v;
},
expression:"dialog"}},


[
_c(
"v-card",
{staticClass:"mx-auto"},
[
_c("v-card-title",{staticClass:"text-h5 text-dark"},[
_vm._v("\n          Are you sure?\n        ")]),

_vm._v(" "),
_c("v-card-text",{staticClass:"text-dark"},[
_vm._v(
"\n          Only submit a task when you are sure it is complete. You cannot edit your work once it has been submitted for review.\n        ")]),


_vm._v(" "),
_c(
"v-card-actions",
{staticClass:"pb-3"},
[
_c("v-spacer"),
_vm._v(" "),
_c(
"v-btn",
{
attrs:{color:"red darken-1",text:""},
on:{
click:function click($event){
_vm.dialog=false;
}}},


[_vm._v("\n            Close\n          ")]),

_vm._v(" "),
_c(
"v-btn",
{attrs:{color:"green"},on:{click:_vm.submit}},
[_vm._v("\n            Submit\n          ")])],


1)],


1)],


1)],


1);

};
var _staticRenderFns=[];
_render._withStripped=true;



/***/}}]);

}());
