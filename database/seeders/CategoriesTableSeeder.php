<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        'name' => 'Citizenship'
        ]);
        Category::create([
        'name' => 'Disability'
        ]);
        Category::create([
        'name' => 'LGBTQ'
        ]);
        Category::create([
        'name' => 'Migration'
        ]);
        Category::create([
        'name' => "Women's History"
        ]);
    }
}
