<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect()->to('/nova');
// });

// Pages
Route::get('/', [PageController::class, 'welcome'])->name('welcome');
Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');

Route::get('/blog', [BlogController::class, 'index'])->name('blog.index');
Route::get('/blog/get', [BlogController::class, 'get'])->name('blog.get');
Route::get('/blog/{date}/{slug}', [BlogController::class, 'show'])->name('blog.show');

Route::get('/projects', [ProjectController::class, 'index'])->name('projects.index');
Route::get('/projects/get', [ProjectController::class, 'get'])->name('projects.get');
Route::get('/projects/{slug}', [ProjectController::class, 'show'])->name('projects.show');

Route::get('/assignments/{assignment}/tasks', [ProjectController::class, 'getTasks'])->name('projects.tasks.get');

Route::post('/send-message', [SendMail::class, 'enquiry'])->name('sendMessage');

//Login Area
Route::get('/home', function(){
    if(Auth::user()->role->id == 2){
        return redirect()->to('/users/dashboard');
    }else if(Auth::user()->role->id == 1){
        return redirect()->to('/nova/login');
    }else{
        return redirect()->to('/logout');
    }
})->name('home');

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->to('/');
});

Auth::routes();

// Employee middlware
Route::group(['middleware' => 'App\Http\Middleware\UsersMiddleware'], function(){

    Route::get('/users/{any}', [SpaController::class, 'users'])->where('any', '.*');
    Route::post('/account/update', [UserController::class, 'update'])->name('user-update');
    Route::get('/account/get-user', [UserController::class, 'show'])->name('user-show');
    Route::post('/account/password/update', [UserController::class, 'updatePassword'])->name('update-password');

    // Users Project Routes
    Route::get('/projects/users/get', [ProjectController::class, 'usersProjects'])->name('users-get-projects');
    Route::get('/projects/users/get/{slug}', [ProjectController::class, 'usersProjectGet'])->name('users-get-project');

    // Tasks Routes
    Route::get('/tasks/get/{id}/{slug}', [TaskController::class, 'show'])->name('users-get-task');
    Route::post('/tasks/save/{task}', [TaskController::class, 'update'])->name('users-save-task');
    Route::post('/tasks/submit/{task}', [TaskController::class, 'submit'])->name('users-submit-task');

});